let drawCircle = require("./drawCircle");
let drawP5Circle = require("./test");

class DrawArt {

    /////////////////////constructor/////////////////////
    constructor() {

    }

    /////////////////////Methods/////////////////////
    draw (spectrum, spectrumCol) {
        drawP5Circle(spectrum, spectrumCol)
    }

    /////////////////////GETTER/////////////////////
    /////////////////////SETTER/////////////////////
}

module.exports = new DrawArt();