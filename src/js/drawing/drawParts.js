let config = require('../Config').conf;
let ChangeResolution = require("./ChangeResolution");

const modes = {
    "2dGraph": require("./modes/2dGraph/drawMode"),
    "3dGraph": require("./modes/3dGraph/drawMode"),
    "art": require("./modes/art/drawMode"),
    "beatDetection": require("./modes/beatDetection/drawMode"),
    "experimental": require("./modes/experimental/drawMode"),
    "pattern": require("./modes/pattern/main"),
    "stronghold": require("./modes/stronghold/drawMode"),
}


/*module.exports = () => {
    require("./modes/experimental/fourierSeries").draw();
}*/
module.exports = () => {
    modes[config.mode]();
}
