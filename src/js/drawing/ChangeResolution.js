class ChangeMode {
    constructor() {

        this._overlay = document.getElementById("fullscreenContainer");
        this._windowedContainer = document.getElementById("windowedContainer");
        this._mode = "windowed";


        console.log(this);

        this._width = 1300;
        this._height = 900;


        const self = this;
        document.body.onresize = function () {
            if (self._mode === "fullscreen") {
                this._width = document.body.offsetWidth;
                this._height = document.body.offsetHeight;

                resizeCanvas(this._width, this._height);
            }
        };


        //test buttons
        document.onkeypress = (e) => {
            if (e.keyCode === 102) {
                this.setMode();
            }
            if (e.keyCode === 27 && this._mode === "fullscreen") {
                this.setMode();
            }
        };
    }

    get width() {
        return this._width;
    }

    get height() {
        return this._height;
    }

    setMode() {
        if (document.getElementById('defaultCanvas0')) {
            if (this._mode === "windowed") {
                this._mode = "fullscreen";
                this._setFullscreen();
            } else {
                this._mode = "windowed";
                this._setWindowed();
            }
        }
    }

    _setFullscreen() {
        this._overlay.style.height = "100%";
        this._windowedContainer.style.display = "none";

        this._width = document.body.offsetWidth;
        this._height = document.body.offsetHeight;
        resizeCanvas(this._width, this._height);

        try {
            this._overlay.appendChild(document.getElementById('defaultCanvas0'));
            document.body.style.visibility = "visible";
            document.getElementsByTagName("html")[0].style.overflowX = "hidden";
            document.getElementsByTagName("html")[0].style.overflowY = "hidden";
        } catch (e) {
            console.error(e);
            console.error(document.getElementById('defaultCanvas0'));
            //window.location.reload()
        }
    }

    _setWindowed() {
        this._overlay.style.height = "0";
        this._windowedContainer.style.display = "";
        this._width = 1300;
        this._height = 900;
        resizeCanvas(this._width, this._height);

        try {
            document.getElementById('box').appendChild(document.getElementById('defaultCanvas0'));
            document.body.style.visibility = "visible";
            document.getElementsByTagName("html")[0].style.overflowX = "hidden";
            document.getElementsByTagName("html")[0].style.overflowY = "scroll";
        } catch (e) {
            console.error(e);
            //window.location.reload()
        }
    }
}

module.exports = new ChangeMode();
