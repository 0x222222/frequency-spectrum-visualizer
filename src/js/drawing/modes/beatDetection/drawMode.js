const FFTfromFFT = require("./src/FFTfromFFT");
const fftfromFFT = new FFTfromFFT();
const Overlay = require("./src/Overlay");
const drawSpectrumSliceOT = require("./src/drawSpectrumSliceOverTime");
const drawSpectrumSliceOS = require("./src/drawSpectrumSliceOverSpace");
const SpectrumStorage = require("./src/SpectrumStorage");


module.exports = () => {

    background([30,30,30]);

    SpectrumStorage.collect();

    fftfromFFT.draw();
    drawSpectrumSliceOT();
    drawSpectrumSliceOS();
    Overlay.draw();
};