const history = require('../../../../History');

module.exports = () => {
    let arr = history.lastZoomed;


    fill([30, 30, 30]);
    strokeWeight(2);
    stroke(0);

    let width = 1024 / arr.length

    beginShape();
    vertex(0, 768);

    for (let i = 0; i < arr.length; i++) {
        vertex(i * width, 768 - arr[i]);
        vertex((i + 1) * width, 768 - arr[i]);
    }

    vertex(1024, 768);
    vertex(0, 768);

    endShape();
}