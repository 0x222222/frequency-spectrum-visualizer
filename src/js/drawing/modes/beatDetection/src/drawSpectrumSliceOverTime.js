const SpectrumStorage = require("./SpectrumStorage")

module.exports = () => {
    let arr = SpectrumStorage.lastN(128);

    fill([30, 30, 30]);
    strokeWeight(2);
    stroke(0);

    let width = 1024 / arr.length

    vertex(0, 512);

    for (let i = 0; i < arr.length; i++) {
        vertex(i * width, 512 - arr[i]);
        vertex((i + 1) * width, 512 - arr[i]);
    }

    vertex(1024, 512);
    vertex(0, 512);

    endShape();
}