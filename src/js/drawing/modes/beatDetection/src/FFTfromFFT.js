const fft = require('fft-js').fft;
const fftUtil = require('fft-js').util;
let history = require('../../../../History');
let _ = require("lodash");
let SpectrumStorage = require("./SpectrumStorage")

class FFTfromFFT {

    /////////////////////constructor/////////////////////
    constructor() {
        console.log(this);
    }

    /////////////////////Methods/////////////////////


    draw() {

        try {

            if (SpectrumStorage.last().length < 128) {
                return;
            }

            const signal = _.clone(SpectrumStorage.lastN(128));
            const phasors = fft(signal);
            const freq = fftUtil.fftFreq(phasors);
            const magnitudes = fftUtil.fftMag(phasors);

            beginShape();

            fill([30, 30, 30]);
            strokeWeight(2);
            stroke(0);


            for (let i = 0; i < this._frameSize / 4; i++) {
                vertex(i * 4096 / this._frameSize, 256 - magnitudes[i] / 8);
            }

            vertex(1024, 0);
            vertex(0, 0);
            endShape();
        } catch (e) {
            debugger
        }
    }

    makeLog(arr) {
        for (let i = 0; i < arr.length; i++) {
            arr[i] = Math.log(arr[i] + 1) * 64;
            if (arr[i] < 0) arr[i] = 0;
        }
    }

    /////////////////////GETTER/////////////////////
    /////////////////////SETTER/////////////////////
}

module.exports = FFTfromFFT;