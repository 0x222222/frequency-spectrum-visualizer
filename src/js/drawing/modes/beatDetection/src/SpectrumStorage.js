let history = require('../../../../History');

class SpectrumStorage {

    /////////////////////constructor/////////////////////
    constructor(targetIndex) {
        this._target = targetIndex || 50;
        this._maxSize = 1024;
        this._storage = [];
    }

    /////////////////////Methods/////////////////////
    collect() {
        this._storage.unshift(history.lastZoomed[this._target]);

        if (this._storage.length > this._maxSize) {
            this._storage.pop();
        }
    }

    lastN(n) {
        return this._storage.slice(0,n);
    }

    last() {
        return this._storage;
    }

    lastNInterpolated(n, size) {

    }

    /////////////////////GETTER/////////////////////
    /////////////////////SETTER/////////////////////
}

module.exports = new SpectrumStorage(50);