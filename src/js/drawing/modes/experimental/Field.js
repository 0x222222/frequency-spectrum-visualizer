class Field {
    constructor(width, height) {
        this._matrix = [];
        this._matrix_blur = [];
        this.width = width;
        this.height = height;
        this.image = new p5.Image(this.width, this.height);

        //Create matrix
        for (let i = 0; i < width; i++) {
            let arr = [];
            let arr_blur = [];
            for (let j = 0; j < height; j++) {
                arr.push(0);
                arr_blur.push(0);
            }
            this._matrix.push(arr);
            this._matrix_blur.push(arr_blur);
        }
    }

    setOne(x, y) {
        if (x >= this.width || y >= this.height) {
            return;
        }

        try {

            this._matrix[x][y] = 1;
        } catch (e) {
            debugger
        }
    }

    get matrix() {
        return this._matrix;
    }


    draw() {

        this.image.loadPixels();

        for (let y = 0; y < this.height; y++) {
            for (let x = 0; x < this.width; x++) {
                let index = (x + y * this.height) * 4;
                let val = this._matrix[x][y] * 255;

                this.image.pixels[index] = val;
                this.image.pixels[index + 1] = val;
                this.image.pixels[index + 2] = val;
                this.image.pixels[index + 3] = 255;
            }
        }

        this.image.updatePixels();

        image(this.image, 0, 0)
    }

    getNeighbours8(x, y, offset) {
        offset = offset || 1;

        return [
            this._matrix[x][y - offset],
            this._matrix[x + offset][y - offset],
            this._matrix[x + offset][y],
            this._matrix[x + offset][y + offset],
            this._matrix[x][y + offset],
            this._matrix[x - offset][y + offset],
            this._matrix[x - offset][y],
            this._matrix[x - offset][y - offset]
        ];
    }

    getNeighbours(x, y) {
        let arr = [];
        let pos = 0;

        for (let i = -1; i <= 1; i++) {
            for (let j = -1; j <= 1; j++) {
                let index_x = x + i;
                let index_y = y + j;

                if (index_x < 0 || index_y < 0 || index_x >= this.width || index_y >= this.height) {
                    pos++;
                    continue;
                }


                arr.push({
                    val: this._matrix[index_x][index_y],
                    pos: pos,
                })

                pos++;
            }
        }

        return arr;
    }

    reduce(val) {
        for (let i = 0; i < this.width; i++) {
            for (let j = 0; j < this.height; j++) {
                this._matrix[i][j] *= val;
            }
        }
    }

    blur() {
        //inner area
        for (let x = 1; x < this.width - 1; x++) {
            for (let y = 1; y < this.height - 1; y++) {
                this._matrix_blur[x][y] = (this.matrix[x + 1][y] + this.matrix[x - 1][y] + this.matrix[x][y + 1] + this.matrix[x][y - 1] + this.matrix[x + 1][y + 1] + this.matrix[x + 1][y - 1] + this.matrix[x - 1][y + 1] + this.matrix[x - 1][y - 1]) / 8
            }
        }


        for (let x = 1; x < this.width - 1; x++) {
            //top row
            this._matrix_blur[x][0] = (this.matrix[x + 1][0] + this.matrix[x - 1][0] + this.matrix[x + 1][1] + this.matrix[x - 1][1] + this.matrix[x][1]) / 5;
            //bottom row
            this._matrix_blur[x][this.height - 1] = (this.matrix[x + 1][this.height - 1] + this.matrix[x - 1][this.height - 1] + this.matrix[x + 1][this.height - 2] + this.matrix[x - 1][this.height - 2] + this.matrix[x][this.height - 2]) / 5;
        }

        for (let y = 1; y < this.height - 1; y++) {
            //top row
            this._matrix_blur[0][y] = (this.matrix[0][y + 1] + this.matrix[0][y - 1] + this.matrix[1][y - 1] + this.matrix[1][y] + this.matrix[1][y + 1]) / 5;
            //bottom row
            this._matrix_blur[this.width - 1][y] = (this.matrix[this.width - 1][y + 1] + this.matrix[this.width - 1][y - 1] + this.matrix[this.width - 2][y - 1] + this.matrix[this.width - 2][y] + this.matrix[this.width - 2][y + 1]) / 5;
        }


        let tmp = this._matrix_blur;
        this._matrix_blur = this._matrix;
        this._matrix = tmp;
    }
}

module.exports = Field;