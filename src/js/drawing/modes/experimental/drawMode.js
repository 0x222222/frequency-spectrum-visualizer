let Agent = require("./Agent");
let Field = require("./Field");
const field = new Field(600,600);

let agents = [];

for (let i = 0; i < 15000; i++) {
    agents.push(new Agent(field));
}

module.exports = () => {

    background(0);
    stroke(255);
    color(255);

    field.draw();
    field.reduce(0.9);

    agents.forEach(agent => {
        agent.move();
        agent.draw();
    })

    field.blur();
}