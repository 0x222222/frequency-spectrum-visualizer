class Agent {
    constructor(field, conf) {

        conf = conf || {};

        this.posX = Math.random() * field.width;
        this.posY = Math.random() * field.height;
        this.direction = Math.random() * 100;

        this.posX = (Math.round(Math.random()) * 2 - 1) * 150 + 300
        this.posY = (Math.round(Math.random()) * 2 - 1) * 150 + 300


        this.areaWidth = field.width;
        this.areaHeight = field.height;
        this.speed = 1 + Math.random() / 3;
        this.field = field;

        this._pseudoRandom = [];
        for (let i = 0; i < 10000; i++) {
            this._pseudoRandom.push(Math.random());
        }
        this._pseudoRandom_pointer = 0;
    }

    //returns int pos x and y
    get exactPosition() {
        return {
            x: Math.round(this.posX),
            y: Math.round(this.posY),
        }
    }

    markOnField() {
        let pos = this.exactPosition;
        this.field.setOne(pos.x, pos.y);
    }

    //move
    move() {
        let newPos = this._getNewPos();

        if (newPos.x < 5 || newPos.y < 5 || newPos.x > this.areaWidth - 5 || newPos.y > this.areaHeight - 5) {
            this.direction = Math.random() * 100;
            return;
        }
        /*if (newPos.x < 5) {
            this.direction = Math.random() + 50;

            if (this.direction >= 100) this.direction -= 100;
            if (this.direction < 0) this.direction += 100;
            return;
        }
        if (newPos.y < 5) {
            this.direction = Math.random() + 50;

            if (this.direction >= 100) this.direction -= 100;
            if (this.direction < 0) this.direction += 100;
            return;
        }
        if (newPos.x > this.areaWidth - 5) {
            this.direction = Math.random() + 50;

            if (this.direction >= 100) this.direction -= 100;
            if (this.direction < 0) this.direction += 100;
            return;
        }
        if (newPos.y > this.areaHeight - 5) {
            this.direction = Math.random() + 50;

            if (this.direction >= 100) this.direction -= 100;
            if (this.direction < 0) this.direction += 100;
            return;
        }*/

        this.posX = newPos.x;
        this.posY = newPos.y;

        this.setNewOrientation();
        this.markOnField()
    }

    setNewOrientation() {

        if (frameCount % 1000 > 950) {
            //return;
        }

        let pos = this.exactPosition;
        let neighbours = this.field.getNeighbours8(pos.x, pos.y, 4);

        let lowest = neighbours[0];
        let lowestIndex = 0;

        neighbours = this.shuffle(neighbours);

        for (let i = 1; i < 8; i++) {
            if (lowest > neighbours[i]) {
                lowestIndex = i;
                lowest = neighbours[i];
            }
        }

        this.direction = lowestIndex * 12.5 + (this._pseudoRandom[this._pseudoRandom_pointer] - 0.5) * 10;
        this._pseudoRandom_pointer++;
        if (this._pseudoRandom_pointer === 10000) this._pseudoRandom_pointer = 0;
        if (this.direction >= 100) this.direction -= 100;
        if (this.direction < 0) this.direction += 100;

    }

    shuffle(a) {
        let j, x, i;
        for (i = a.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = a[i];
            a[i] = a[j];
            a[j] = x;
        }
        return a;
    }

    draw() {
        let pos = this.exactPosition;
        point(pos.x, pos.y)
    }

    //
    _getNewPos() {
        let xMove, yMove, percentageX, multiply;

        if (this.direction < 25) {
            xMove = 1;
            yMove = -1;
            percentageX = 0.04 * this.direction;
        } else if (this.direction < 50) {
            xMove = 1;
            yMove = 1;
            percentageX = 1 - 0.04 * (this.direction - 25);
        } else if (this.direction < 75) {
            xMove = -1;
            yMove = 1;
            percentageX = 0.04 * (this.direction - 50);
        } else {
            xMove = -1;
            yMove = -1;
            percentageX = 1 - 0.04 * (this.direction - 75);
        }


        multiply = (100 - Math.abs(percentageX * 100 - 50) * 2) * 0.0041 + 1;

        return {
            x: xMove * percentageX * this.speed * multiply + this.posX,
            y: yMove * (1 - percentageX) * this.speed * multiply + this.posY,
        }
    }
}

module.exports = Agent;
