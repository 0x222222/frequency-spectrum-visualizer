let history = require('../../../../History');
let res = require('../../../ChangeResolution');

let mid = 0;

module.exports = function () {
    let midX = Math.round(res.width / 2);
    let midY = Math.round(res.height / 2);


    fill(0)
    square(0, 0, res.width, res.height);

    let spectrum = history.lastFiltered;
    let spectrumCol = history.lastFilteredColored;


    let sum = 0;
    for (let i = 0; i < 8; i++) {
        sum += spectrum[i];
    }
    let mean = sum / 8;

    mid += mean / 5

    if (mid > res.width) mid = 0;


    let factor = 2;

    for (let i = 0; i < (res.height / 2) && i < 1024; i++) {
        if(spectrum[i]<5) continue;
        stroke(spectrumCol[i]);

        //left
        line(mid, midY - (i / factor), res.width, midY - i)
        line(mid, midY + (i / factor), res.width, midY + i)

        //right
        line(0, midY - i, mid, midY - (i / factor))
        line(0, midY + i, mid, midY + (i / factor))
    }

}

