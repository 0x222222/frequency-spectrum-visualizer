let conf = require('../../../../Config').conf
let history = require('../../../../History');
let res = require('../../../ChangeResolution');

module.exports = function () {
    let midX = Math.round(res.width / 2);
    let midY = Math.round(res.height / 2);

    let spectrumCol = history.lastFilteredColored;

    noFill();



    for (let i = 0; i < 650; i++) {

        let cornerX = midX - i
        let cornerY = midY - i


        //get color
        // let sum_r = 0;
        // let sum_g = 0;
        // let sum_b = 0;
        // let count = 0;
        //
        //
        // for (let j = -1; j <= 1; j++) {
        //     if (i+j <0 || i+j > spectrumCol.length) {
        //         continue;
        //     }
        //
        //     sum_r += spectrumCol[i+j][0];
        //     sum_g += spectrumCol[i+j][1];
        //     sum_b += spectrumCol[i+j][2];
        //     count++;
        // }
        //
        // let mean_r = sum_r / count;
        // let mean_g = sum_g / count;
        // let mean_b = sum_b / count;

        //stroke([mean_r, mean_g, mean_b]);
        stroke(spectrumCol[i])

        square(cornerX, cornerY, 1 + i * 2, 1 + i * 2);
    }
}
