let makeNoise2D = require("open-simplex-noise").makeNoise2D;
let history = require('../../../../History');

class Particle {

    /////////////////////constructor/////////////////////
    constructor(posX, posY, index) {
        this._noise = makeNoise2D(Math.random());
        this._randomoffset = Math.random();
        this._randomCylceOffset = Math.random() / 300;

        this._posXOrigin = posX;
        this._posYOrigin = posY;
        this._posX = posX;
        this._posY = posY;
        this._index = index;
        this._cylce = 0;
    }

    /////////////////////Methods/////////////////////
    move(spectrum) {
        this._cylce += Math.pow(spectrum[this._index] / 16, 2) / 6000 + this._randomCylceOffset;
        let offsetX = this._noise(this._cylce + this._randomoffset, 0) * 325 + this._noise(this._cylce * 10 + this._randomoffset, 0) * 12;
        let offsetY = this._noise(0, this._cylce + this._randomoffset) * 325 + this._noise(0, this._cylce * 10 + this._randomoffset) * 12;

        this._posX = Math.ceil(this._posXOrigin + offsetX);
        this._posY = Math.ceil(this._posYOrigin + offsetY);


        let distance = Math.hypot(650 - this._posX, 450 - this._posY);
        this._index = Math.ceil(1023 / 790 * distance);
    }

    draw(spectrum, spectrumCol) {


        if (spectrum[this._index] < 140) {
            if (this._frameLives === 0) {

                return
            } else {
                this._frameLives--;
            }
        }
        this._frameLives = 8;


        let size = Math.ceil(spectrum[this._index] / 128);

        strokeWeight(size);

        stroke(spectrumCol[this._index]);
        fill(spectrumCol[this._index]);
        point(this._posX, this._posY);
        //circle(this._posX, this._posY, 3);
    }

    /////////////////////GETTER/////////////////////
    /////////////////////SETTER/////////////////////
}

module.exports = Particle;