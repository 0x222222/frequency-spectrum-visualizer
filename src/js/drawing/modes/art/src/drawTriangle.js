let conf = require('../../../../Config').conf
let history = require('../../../../History');
let res = require('../../../ChangeResolution');
let shift = 0;

module.exports = function () {
    background([0,0,0,25])
    let midX = Math.round(res.width / 2);
    let midY = Math.round(res.height / 2);
    let midY_shift = Math.round(res.height / 2);


    let spectrumCol = history.lastFilteredColored;
    let spectrum = history.lastFiltered;

    let sum = 0;
    shift += spectrum[10] / 20000 + spectrum[3] / 20000 +spectrum[15] / 20000;


    let side = 0;

    for (let i = 750; i >= 0; i--) {
        let points = [];
        for (let j = 0; j < 3; j++) {
            let plusMinus = i % 80 < 40 ? 1 : -1;

            let ihead = Math.sin(2.094 * (j + 0.5+shift)*plusMinus);
            let jhead = Math.cos(2.094 * (j + 0.5+shift)*plusMinus);

            points.push([midX + ihead * (i + 50), midY_shift + jhead * (i + 50)]);
        }

        stroke(spectrumCol[i]);

        triangle(points[0][0], points[0][1], points[1][0], points[1][1], points[2][0], points[2][1]);
    }
}
