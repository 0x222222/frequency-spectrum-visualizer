let conf = require('../../../../Config').conf
let history = require('../../../../History');
let makeNoise2D = require("open-simplex-noise").makeNoise2D;
let res = require('../../../ChangeResolution');
let noise = makeNoise2D(3);

module.exports = function () {
    noFill();

    let midX = Math.round(res.width / 2);
    let midY = Math.round(res.height / 2);

    let spectrumCol = history.lastFilteredColored;

    let offsetX = noise(frameCount / 80, 0);
    let offsetY = noise(0, frameCount / 80);


    for (let i = 8; i < 1024; i++) {
        stroke(spectrumCol[i]);

        let eyeFactor = 32 / i;
        eyeFactor = eyeFactor > 0.7 ? 0.7 : eyeFactor;

        let offsetX = noise(frameCount / 80, 0) * eyeFactor * 20;
        let offsetY = noise(0, frameCount / 80) * eyeFactor * 20;


        ellipse(midX + offsetX, midY + offsetY, i * 1.55, i * (1.55 - eyeFactor))
    }

    for (let j = 0; j < 16; j += 2) {



        stroke(spectrumCol[j]);

        let i = j / 2;

        let eyeFactor = 32 / i;
        eyeFactor = eyeFactor > 0.7 ? 0.7 : eyeFactor;

        let offsetX = noise(frameCount / 80, 0) * eyeFactor * 20;
        let offsetY = noise(0, frameCount / 80) * eyeFactor * 20;

        eyeFactor = 0;

        ellipse(midX + offsetX, midY + offsetY, i * 1.55, i * (1.55 - eyeFactor))
    }
}
