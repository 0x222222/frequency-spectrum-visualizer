let conf = require('../../../../Config').conf
let history = require('../../../../History');
let res = require('../../../ChangeResolution');

let pulseHistory = [];

module.exports = function () {
    background(0);
    noFill();

    let midX = Math.round(res.width / 2);
    let midY = Math.round(res.height / 2);

    let spectrum = history.lastFiltered;


    let sum;
    let meanLF = 0, meanMF = 0, meanHF = 0;


    sum = 0;
    for (let i = 0; i < 64; i++) {
        sum += spectrum[i];
    }
    meanLF = sum / 64;

    sum = 0;
    for (let i = 64; i < 512; i++) {
        sum += spectrum[i];
    }
    meanMF = sum / 448;

    sum = 0;
    for (let i = 512; i < 1024; i++) {
        sum += spectrum[i];
    }
    meanHF = sum / 512;

    pulseHistory.unshift([
        meanLF,
        meanMF,
        meanHF,
    ]);

    while (pulseHistory.length > 1024) pulseHistory.pop();

    //fill(pulseHistory[0]);

    let mid_size = 122;

    for (let i = 0; i < 25; i++) {
        stroke([
            pulseHistory[0][0]-i*10,
            pulseHistory[0][1]-i*10,
            pulseHistory[0][2]-i*10,
        ]);
        circle(midX, midY, mid_size-i);
    }
    noFill();

    let hf, mf, lf;

    for (let i = 0; i < 1024 && i < pulseHistory.length; i++) {

       let degrease = Math.trunc(i / 4);

       stroke([
           pulseHistory[i][0] - degrease,
           pulseHistory[i][1] - degrease,
           pulseHistory[i][2] - degrease
       ]);

       circle(midX, midY, i * 2 + 1 + mid_size);
       circle(midX, midY, i * 2 + 2 + mid_size);
    }
    // for (let i = 0; i < 512 && i < pulseHistory.length; i++) {
    //
    //     let degrease = Math.trunc(i / 4);
    //
    //     stroke([pulseHistory[i][0] - degrease, 0, 0]);
    //     circle(midX, midY, i * 5 + 5);
    //     stroke([0, pulseHistory[i][1] - degrease, 0]);
    //     circle(midX, midY, i * 5 + 6);
    //     stroke([0, 0, pulseHistory[i][2] - degrease]);
    //     circle(midX, midY, i * 5 + 7);
    // }
}
