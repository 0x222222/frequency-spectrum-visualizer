let history = require('../../../../History');
let res = require('../../../ChangeResolution');

class drawFreqCircle {

    /////////////////////constructor/////////////////////
    constructor() {
        this.colorWhell = [];

        for (let i = 0; i < 256; i++) {
            let arr = [];
            for (let j = 0; j < 360; j++) {
                arr.push(this.hsv2rgb(j, 5, i))
            }

            this.colorWhell.push(arr)
        }

        this._rotation = 0;
    }

    /////////////////////Methods/////////////////////

    hsv2rgb(h, s, v) {
        let f = (n, k = (n + h / 60) % 6) => v - v * s * Math.max(Math.min(k, 4 - k, 1), 0);
        return [f(5) * 255, f(3) * 255, f(1) * 255];
    }

    draw() {
        if (frameCount % 25 === 0) {
            background([0, 0, 0, 12]);
        } else {
            background([0, 0, 0, 12]);
        }

        const spectrum = history.lastFiltered;
        const spectrumCol = history.lastFilteredColored;
        let midX = Math.round(res.width / 2);
        let midY = Math.round(res.height / 2);


        let step = PI / 512;


        let all = 0;
        for (let i = 0; i < 1024; i += 4) {
            all += spectrum[i];
        }
        let mean = Math.trunc(all / 256);

        this._rotation += mean * 0.001 / 64;

        for (let circle = 0; circle < 4; circle++) {
            beginShape();

            let expo = 0.4 + (circle / 10);

            let index = 132;
            let ih = 0;
            let jh = 0;

            let color = this.colorWhell[mean][(frameCount + 60 * circle) % 360];
            stroke(
                color[0]* 0.0125,
                color[1]* 0.0125,
                color[2]* 0.0125,
            );

            for (let i = 0; i < 1024; i += 2) {

                if (i % 256 < 128) {
                    index -= 2;
                } else {
                    index += 2;
                }

                ih = Math.sin(this._rotation + i * step);
                jh = Math.cos(this._rotation + i * step);

                vertex(
                    (spectrum[index] * expo) * ih + (ih * 64 * (circle + 1)) + midX,
                    (spectrum[index] * expo) * jh + (jh * 64 * (circle + 1)) + midY
                )

            }
            endShape(CLOSE);
        }

    }

    /////////////////////GETTER/////////////////////
    /////////////////////SETTER/////////////////////
}

module.exports = new drawFreqCircle();
