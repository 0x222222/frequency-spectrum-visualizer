let conf = require('../../../../Config').conf
let history = require('../../../../History');
let makeNoise2D = require("open-simplex-noise").makeNoise2D;
let res = require('../../../ChangeResolution');
let noise = makeNoise2D(3);

module.exports = function () {
    background([0,0,0,25])

    if(frameCount%50 ===0){
        background([0,0,0,75])
    }

    let width = res.width;
    let height = res.height;
    let midX = Math.round(res.width / 2);
    let midY = Math.round(res.height / 2);


    let spectrumCol = history.lastFilteredColored;
    let spectrum = history.lastFiltered;


    for (let i = 8; i < 1024; i++) {
        let val = spectrum[i];

        if(Math.random() * 255 < val){
            //let circlePos = Math.random()*Math.Pi*2;
            //let distance = i+5*Math.random();
            //let x = distance*Math.cos(circlePos)+midX;
            //let y = distance*Math.sin(circlePos)+midY;

            let x = Math.trunc(Math.random() * width);
            ////let y = height-(Math.trunc(Math.random() * height)* i / 1024);
            let y = Math.trunc(Math.random()*height);

            color(spectrumCol[i]);
            fill(spectrumCol[i]);

            let count = Math.ceil(val/25);

            for (let j = 0; j <count;  j++) {

                circle(x,y, Math.ceil(val/25));
            }
        }
    }
}
