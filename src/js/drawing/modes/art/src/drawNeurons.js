let conf = require('../../../../Config').conf
let history = require('../../../../History');
let res = require('../../../ChangeResolution');
let makeNoise2D = require("open-simplex-noise").makeNoise2D;
let noise = makeNoise2D(Date.now());

class drawNeurons {
    constructor() {
        //conf
        this._neuronCount = 3000;
        this._areaSize = 250;

        //runtime
        this._neurons = [];
        this._cons = [];
        this._drawed = false;

        let a = Date.now();
        this._createNeurons()
        this._buildCons()
        let b = Date.now();
        console.log(b - a)
    }

    _createNeurons() {
        while (this._neurons.length < this._neuronCount) {

            const x = Math.trunc(Math.random() * 1300);

            const y = Math.trunc(Math.random() * 900);


            const val = 1023 - Math.pow(noise(x / this._areaSize, y / this._areaSize) + 1, 1) * 512;

            //if (val > Math.random() * 1200) continue;
            if (Math.pow(noise(x / this._areaSize, y / this._areaSize) + 1, 3) < Math.random()) continue;

            this._neurons.push({x, y, val})
        }

        this._neurons.sort((a, b) => {
            return a.val - b.val;
        })

        const lowest = this._neurons[0].val;
        const highest = this._neurons[this._neuronCount - 1].val - lowest;
        const multiplayer = 1023 / highest;

        this._neurons.forEach(neuron => {
            neuron.val -= lowest;
            neuron.val *= multiplayer;
            neuron.val = Math.trunc(neuron.val);
            if (neuron.val < 5) neuron.val = 5;
        })
    }

    _buildCons() {

        let distanceMap = [];

        for (let i = 0; i < this._neuronCount; i++) {
            distanceMap.push([]);
        }

        const ns = this._neurons;

        for (let i = 0; i < this._neuronCount; i++) {
            for (let j = i + 1; j < this._neuronCount; j++) {
                let distance = Math.hypot(ns[i].x - ns[j].x, ns[i].y - ns[j].y);

                if (distance > 80) continue;
                if (distance < 20) continue;

                distanceMap[i].push({distance, index: j});
                distanceMap[j].push({distance, index: i});
            }
        }


        for (let i = 0; i < this._neuronCount; i++) {
            distanceMap[i].sort((a, b) => {
                return a.distance - b.distance;
            });
            distanceMap[i] = distanceMap[i].splice(1, 5);
        }

        let connections = {};

        for (let i = 0; i < this._neuronCount; i++) {
            let amount = Math.trunc(Math.random() * 2) + 1;

            for (let j = 0; j < amount && j < distanceMap[i].length; j++) {
                let firstIndex = i;
                let secondIndex = distanceMap[i][j].index;

                if (firstIndex > secondIndex) {
                    let temp = firstIndex;
                    firstIndex = secondIndex;
                    secondIndex = temp;
                }

                let key = firstIndex + "," + secondIndex;
                connections[key] = distanceMap[i][j].distance;
            }
        }

        for (const key in connections) {
            const i0 = key.split(",")[0];
            const i1 = key.split(",")[1];
            const distance = connections[key];

            this._cons.push([
                ns[i0],
                ns[i1],
                distance,
                Math.trunc((ns[i0].val + ns[i1].val) / 2),
            ]);
        }
    }

    _move() {

        let target = (5 + Math.round(frameCount/ 800))%30;

        for (let i = 0; i < 64; i++) {
            const pos = Math.trunc(Math.random() * this._neuronCount);


            const x = noise(1, this._neurons[pos].x/target);
            const y = noise(this._neurons[pos].y/target, 1);

            this._neurons[pos].x += x;
            this._neurons[pos].y += y;
        }
    }

    draw() {

        this._move();

        background([0, 0, 0, 30])
        strokeWeight(1);

        let spectrumCol = history.lastFilteredColored;
        let spectrum = history.lastFiltered;


        this._cons.forEach(con => {
            const val = spectrum[con[3]];
            if (val < 64) return;


            stroke(spectrumCol[con[3]]);
            line(con[0].x, con[0].y, con[1].x, con[1].y);
        })

        this._neurons.forEach(neuron => {
            fill(64 + spectrum[neuron.val] / 2)
            circle(neuron.x, neuron.y, 4)
        })
    }
}

module.exports = new drawNeurons();
