let conf = require('../../../../Config').conf
let history = require('../../../../History');
let res = require('../../../ChangeResolution');
let Particle = require("./Particle");

class DrawParticle {
    constructor() {
        this._particles = [];


        while (this._particles.length <= 1500) {

            let x = Math.floor(Math.random() * res.width);
            let y = Math.floor(Math.random() * res.height);
            let maxDistance = Math.hypot(res.width / 2, res.height / 2);
            let distance = Math.hypot(res.width / 2 - x, res.height / 2 - y);
            let index = Math.ceil(1023 / maxDistance * distance);

            if (Math.random() < distance / maxDistance) {
                continue;
            }
            if (0.5 < distance / maxDistance) {
                continue;
            }

            this._particles.push(new Particle(x, y, index))
        }
    }

    draw() {
        if (frameCount === 1) background(0);

        background([0, 0, 0, 15])
        let spectrumCol = history.lastFilteredColored;
        let spectrum = history.lastFiltered;


        this._particles.forEach(particle => {
            /*  if (Math.random() < 0.1) {

              }*/

            particle.move(spectrum);
            particle.draw(spectrum, spectrumCol);
        });
    }
}

module.exports = new DrawParticle();
