let history = require('../../../../History');
let res = require('../../../ChangeResolution');

module.exports = function () {
    let midX = Math.round(res.width / 2);
    let midY = Math.round(res.height / 2);


    fill([0,0,0,64])
    //fill([150, 230, 150, 32])
    square(0, 0, res.width, res.height);

    let spectrum = history.lastFiltered;
    let spectrumCol = history.lastFilteredColored;


    let sum = 0;
    spectrum.forEach(val => {
        sum += val
    })
    let mean = sum / 1024;

    const rotations = (3 + mean / 256) * 1;
    const arms = 6;

    let step = PI / (1024 / rotations);

    noFill();

    strokeWeight(2);

    for (let j = 0; j < arms; j++) {
        let offset = PI * j * 2 / arms;


        for (let i = 0; i < 1024; i++) {

            if (i % 256 === 0) {
                stroke(spectrumCol[i+32]);
                beginShape();
            }

            ih = Math.sin(offset + i * step);
            jh = Math.cos(offset + i * step);

            vertex(
                (spectrum[i] / 4) * ih + (i / 2) * ih + midX,
                (spectrum[i] / 4) * jh + (i / 2) * jh + midY
            )

            if (i % 256 === 255) {
                endShape();
            }
        }
    }


}


// module.exports = function () {
//    let midX = Math.round(res.width / 2);
//    let midY = Math.round(res.height / 2);
//
//    let spectrum = history.lastFiltered;
//    let step = PI / 51;
//
//    beginShape();
//
//    for (let i = 0; i < 1024; i++) {
//
//        //if(i%2===0) continue;
//
//        ih = Math.sin( + i * step);
//        jh = Math.cos( + i * step);
//
//        vertex(
//            spectrum[i]  * (i/2) * ih + midX,
//            spectrum[i]  * (i/2) * jh + midY
//        )
//    }
//
//    endShape();
// }
