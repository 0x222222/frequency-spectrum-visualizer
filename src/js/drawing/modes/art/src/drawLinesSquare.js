let history = require('../../../../History');
let res = require('../../../ChangeResolution');


module.exports = function () {
    let midX = Math.round(res.width / 2);
    let midY = Math.round(res.height / 2);


    fill(0)
    square(0, 0, res.width, res.height);

    let spectrum = history.lastFiltered;
    let spectrumCol = history.lastFilteredColored;

    let sum = 0;
    spectrum.forEach(val => {
        sum += val
    })
    let mean = sum / 1024;



    for (let i = 0; i < 400; i++) {
        if (spectrum[i] < 10) continue;
        stroke(spectrumCol[i]);
        line(0, midY - i, midX-mean*3-50, midY - (i / 2))
        line(0, midY + i, midX-mean*3-50, midY + (i / 2))
    }

    for (let i = 0; i < 400; i++) {
        if (spectrum[i] < 10) continue;
        stroke(spectrumCol[i]);
        line(midX+mean*3+50, midY - (i / 2) , res.width, midY - i)
        line(midX+mean*3+50, midY + (i / 2) , res.width, midY + i)
    }

    for (let i = 0; i < mean*3+50; i++) {

        stroke(spectrumCol[i]);
        line(midX-i,0,midX-i,res.height);
        line(midX+i,0,midX+i,res.height);
    }
}

