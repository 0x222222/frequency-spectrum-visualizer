let conf = require('../../../../Config').conf
let history = require('../../../../History');
let res = require('../../../ChangeResolution');
//const {checkIntersection} = require('line-intersect');
const zlib = require("zlib");

class drawNeurons {
    constructor() {
        //options
        this._firstPointCount = 10;
        this._neuronCount = 1200;
        this._shapreness = 64;
        this._distanceMax = 80;
        this._distanceMin = 30;


        //runtime variables
        this._neurons = [];
        this._connections = [];
        this._drawed = false;

        this._createPoint();
        this._buildConnections();

    }

    _createPoint() {
        let a = Date.now();
        console.log("Start generate");

        const w = 1300;
        const h = 900;

        //create first points
        const firstPoints = [];

        for (let i = 0; i < this._firstPointCount; i++) {
            firstPoints.push([Math.trunc(Math.random() * w), Math.trunc(Math.random() * h), Math.trunc(i * (1023 / (this._firstPointCount - 1))),]);
        }

        //create main points
        for (let i = 0; i < this._neuronCount; i++) {
            const x = Math.trunc(Math.random() * w);
            const y = Math.trunc(Math.random() * h);

            let valueSum = 0;
            let distanceSum = 0;
            let multiplyerSum = 0;

            for (let j = 0; j < this._firstPointCount; j++) {
                let distance = Math.hypot(x - firstPoints[j][0], y - firstPoints[j][1]);
                distanceSum += distance;
            }

            for (let j = 0; j < this._firstPointCount; j++) {
                let distance = Math.hypot(x - firstPoints[j][0], y - firstPoints[j][1]);
                let value = firstPoints[j][2];

                const multiplyer = (distanceSum - distance) ** this._shapreness;

                valueSum += value * multiplyer;
                multiplyerSum += multiplyer;
            }


            const value = valueSum / multiplyerSum;

            this._neurons.push([x, y, value,])
        }

        //expand to full range
        let lowest = Infinity;
        let highest = 0;

        this._neurons.forEach(neuron => {
            if (lowest > neuron[2]) lowest = neuron[2];
            if (highest < neuron[2]) highest = neuron[2];
        });

        this._neurons.forEach(neuron => {
            neuron[2] -= lowest;
        });

        highest -= lowest;
        let multiplyer = 1023 / highest;

        for (let i = 0; i < this._neuronCount; i++) {
            let neuron = this._neurons[i];

            if (lowest > neuron[2]) lowest = neuron[2];
            if (highest < neuron[2]) highest = neuron[2];
        }

        for (let i = 0; i < this._neuronCount; i++) {
            let neuron = this._neurons[i];

            neuron[2] *= multiplyer;
            neuron[2] = Math.trunc(neuron[2]);
        }

        console.log("Finished generate");

        console.log(Date.now() - a, "ms");
    }

    _buildConnections() {
        //create
        for (let i = 0; i < this._neuronCount; i++) {
            for (let j = 0; j < this._neuronCount; j++) {
                if (i === j) continue;

                const l1 = this._neurons[i];
                const l2 = this._neurons[j];

                const distance = Math.hypot(l1[0] - l2[0], l1[1] - l2[1]);

                if (distance > this._distanceMax || distance < this._distanceMin) continue;

                this._connections.push([l1[0], l1[1], l2[0], l2[1], distance, Math.round((l1[2] + l2[2]) / 2),]);
            }
        }


        this._shuffleArray(this._connections);
        this._connections = this._connections.splice(0,1500);

        //reduce
        /*let noOverlap = false;
        let round = 0;

        while (!noOverlap) {
            console.log("Round: " + ++round);
            noOverlap = true;

            this._shuffleArray(this._connections);

            for (let i = 0; i < this._connections.length; i++) {

                console.log(i + " of " + this._connections.length);
                for (let j = 0; j < this._connections.length; j++) {
                    if (i === j) continue;


                    const c1 = this._connections[i];
                    const c2 = this._connections[j];


                    if (checkIntersection(
                            c1[0],
                            c1[1],
                            c1[2],
                            c1[3],
                            c2[0],
                            c2[1],
                            c2[2],
                            c2[3],
                        ).type !== "none" &&
                        (c1[0] !== c2[0] || c1[1] !== c2[1]) &&
                        (c1[0] !== c2[2] || c1[1] !== c2[3]) &&
                        (c1[2] !== c2[0] || c1[3] !== c2[1]) &&
                        (c1[2] !== c2[2] || c1[3] !== c2[3])
                    ) {
                        this._connections.splice(j, 1);
                        j--;
                        /!*try  {
                            if (c1[4] > c2[4]) {
                                this._connections.splice(j, 1);
                                j--;
                            } else {
                                this._connections.splice(i, 1);
                            }
                        }
                        catch (e){
debugger
                        }*!/
                        noOverlap = false;
                    }
                }
            }
        }*/
    }

    draw() {
        strokeWeight(2)
        if (this._drawed) {
            //return;
        } else {
            this._drawed = true;
            background(0);
        }


        let spectrumCol = history.lastFilteredColored;
        let spectrum = history.lastFiltered;

        const w = 1300;
        const h = 900;

        background([0,0,0,28]);

        /*for (let i = 0; i < this._neuronCount; i++) {
            let neuron = this._neurons[i];
            let value = Math.trunc(neuron[2] / 8);

            if(value<192) continue;

            noStroke()
            fill(spectrum[neuron[2]])

            circle(neuron[0], neuron[1],5);
        }*/

       /* for (let i = 0; i < 32; i++) {
            let x = Math.random() < 0.5 ? 1 : -1;
            let y = Math.random() < 0.5 ? 1 : -1;
            let con
        }*/

        for (let i = 0; i < spectrum.length; i++) {
            spectrumCol[i].push(192)
        }

        this._connections = this._connections.sort((a, b) => {
            return spectrum[b[5]] - spectrum[a[5]]
        })

        for (let i = 0; i < this._connections.length && i < 600; i++) {
            const con = this._connections[i];


            if (spectrum[con[5]] < 64) break;
            stroke(spectrumCol[con[5]]);

            line(
                con[0],
                con[1],
                con[2],
                con[3],
            );
        }

        /*this._connections.forEach(con => {

            if (spectrum[con[5]] < 16) return;

            stroke(spectrum[con[5]]);
            line(
                con[0],
                con[1],
                con[2],
                con[3],
            );
        })*/
    }

    _shuffleArray(array) {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }
}

module.exports = new drawNeurons();
