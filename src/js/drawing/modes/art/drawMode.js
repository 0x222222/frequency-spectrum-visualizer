//require
const conf = require("../../../Config");
let drawBoxes = require('./src/drawBoxes')
let drawCircle = require('./src/drawCircle')
let drawSpiral = require('./src/drawSprial')
let drawLines = require('./src/drawLines')
let drawLinesSquare = require('./src/drawLinesSquare')
let drawTriangle = require('./src/drawTriangle')
let drawDots = require('./src/drawDots')
let drawParticle = require('./src/drawParticle')
let drawFreqCircle = require('./src/drawFreqCircle')
let drawPulse = require('./src/drawPulse')
let drawNeurons = require('./src/drawNeurons')


//all art player require
const CircleAndSquares = require("./artPlayer/CircleAndSquares")

//all art player init
const circleAndSquares = new CircleAndSquares();

//all draw functions
let drawFunctions = {
    circleAndSquares: () => {
        circleAndSquares.draw();
    },
    squares: drawBoxes,
    circle: drawCircle,
    spiral: drawSpiral,
    lines: drawLines,
    linesAndSquares: drawLinesSquare,
    triangle: drawTriangle,
    dots: drawDots,
    freqCircle: () => {
        drawFreqCircle.draw();
    },
    drawParticle: () => {
        drawParticle.draw();
    },
    neurons: () => {
        drawNeurons.draw();
    },
    pulse: () => {
        drawPulse();
    },
}


//function
module.exports = function () {

    const drawFunction = drawFunctions[conf.artMode];
    if (drawFunction == null) throw new Error("Unknown art mode");
    push();
    drawFunction();
    pop();

    //if (frameCount < 250) {
    //    drawLinesSquare()
    //} else if (frameCount < 550) {
    //    let possibility = (frameCount - 250) / 300;
//
    //    if (possibility > Math.random()) {
    //        drawBoxes()
    //    } else {
    //        drawLinesSquare()
    //    }
    //} else {
    //    drawBoxes()
    //}


    //if (artMode_conf.mode >= modes.length) artMode_conf.mode = modes.length - 1;
    //if (artMode_conf.mode < 0) artMode_conf.mode = 0;
    //modes[artMode_conf.mode]();

    //oFill();
    //  background([0, 0, 0]);
    //
    //
    // if (cooldown < 0) {
    //    cooldown = 1;
    //    mode = 1 - mode;
    // } else if (history.lastFiltered[10] > 150) {
    //    cooldown -= history.lastFiltered[10] / 4048
    // }
    //
    // if (mode) {
    //    drawCircle();
    // } else {
    //    drawBoxes();
    // }

    //drawPulse();

    //background([0,0,0,25])
    //drawTriangle()

    //drawDots();

    //background(0)
    //drawPulse()


}
