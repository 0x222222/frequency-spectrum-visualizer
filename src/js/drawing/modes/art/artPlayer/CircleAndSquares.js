let drawBoxes = require('../src/drawBoxes');
let drawCircle = require('../src/drawCircle');
let history = require("../../../../History");

class CircleAndSquares {
    constructor() {
        this.cooldown = 0;
        this.mode = 0;
    }

    draw() {
        background([0, 0, 0]);


        if ( this.cooldown < 0) {
            this.cooldown = 1;
            this.mode = 1 - this.mode;
        } else if (history.lastFiltered[10] > 128) {
            this.cooldown -= history.lastFiltered[10] / 2048
        }

        if (this.mode) {
            drawCircle();
        } else {
            drawBoxes();
        }
    }
}

module.exports = CircleAndSquares;
