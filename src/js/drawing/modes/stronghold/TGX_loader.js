class TGX_loader {

    constructor() {
        this._loaded = false;
        this._parsed = false;
        this._binaryString = "";
        this._width = undefined;
        this._height = undefined;

        this._image = undefined;
    }

    load() {
        if (this._loaded) {
            return;
        }
        const fs = window.require("electron").remote.require("fs");
        let file = fs.readFileSync("C:\\Users\\Henry\\WebstormProjects\\frequency-spectrum-visualizer\\src\\js\\drawing\\modes\\stronghold\\ST31_Hopsfarm.tgx");


        for (let i = 8; i < file.length; i++) {
            this._binaryString += this._hex2bin(file[i]);
        }

        this._width = parseInt(file[0], 16);
        this._height = parseInt(file[4], 16);

        console.log("Width: ", this._width);
        console.log("Height: ", this._height);
        console.log("Height: ", this._binaryString);

        this._image = [];

        for (let i = 0; i < this._width; i++) {
            this._image.push(new Unit(this._height));
        }

        this._loaded = true;
    }

    _hex2bin(hex) {
        return ("00000000" + (parseInt(hex, 16)).toString(2)).substr(-8);
    }

    parse() {
        if (this._parsed) {
            return;
        }

        let x_pos = 0;
        let y_pos = 0;

        //000: Stream-of-pixels
        //100: Newline
        //010: Repeating pixels
        //001: Transparent-Pixel-String
        let pixelMode = undefined;

        //scanMode

        //0 find pixelMode
        //1 get number of pixel
        //2 read color
        let scanMode = 0;

        let pixelToRead = 0;

        let current = "";

        for (let i = 0; i < this._binaryString.length; i++) {
            let bit = this._binaryString[i];
            current += bit.toString();

            if (current.length === 3 && scanMode === 0) {
                if (current === "000") pixelMode = "stream";
                if (current === "100") pixelMode = "newline";
                if (current === "010") pixelMode = "repeat";
                if (current === "001") pixelMode = "transparent";

                current = "";
                scanMode = 1;
            }

            if (current.length === 5 && scanMode === 1) {

                //code

                current = "";
            }

            if (current.length === 16 && scanMode === 2) {

                //code

                current = "";
                pixelToRead--;

                if (pixelToRead === 0) {
                    scanMode = 0;
                }
            }
        }

        this._parsed = true;
    }
}

module.exports = TGX_loader
