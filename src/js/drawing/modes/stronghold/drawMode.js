const TGX_loader = require("./TGX_loader");
const tgx_loader = new TGX_loader();

module.exports = () => {
    tgx_loader.load();
    tgx_loader.parse();
}
