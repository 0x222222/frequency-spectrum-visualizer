const history = require("../../../History");
const colFilter = require("../../../filter/color/heat");

class Recorder {
    constructor() {
        this.lastFrame = new Uint8Array(1024);
        this.frameSlope = new Uint8Array(1024);
        this.image = new p5.Image(256, 256);
        this.threshold = 128;

        this.record = [];

        for (let i = 0; i < 256; i++) {
            const column = [];

            for (let j = 0; j < i; j++) {

                const cell = [];

                for (let k = 0; k < 256; k++) {
                    cell.push(0)
                }

                column.push(cell);
            }

            this.record.push(column);
        }

        console.log(this.record)
    }

    recordFrame() {
        if(frameCount % 4 !== 0) return;

        const spectrum = history.lastFiltered;

        //create frame
        for (let i = 0; i < 1024; i++) {
            const diff = this.lastFrame[i] - spectrum[i];
            spectrum[i] = this.lastFrame[i];

            if (diff < -12) {
                this.frameSlope[i] = 2;
            } else if (diff > 12) {
                this.frameSlope[i] = 1;
            } else {
                this.frameSlope[i] = 0;
            }
        }

        //calc triangle
        for (let i = 0; i < 256; i++) {
            for (let j = 0; j < i; j++) {
                if (this.frameSlope[i * 4] === this.frameSlope[j * 4] && this.frameSlope[i * 4] !== 0) {
                    this.record[i][j].push(1);
                } else {
                    this.record[i][j].push(0);
                }

                this.record[i][j].shift();
            }
        }
    }

    draw() {
        background(0)
        this.image.loadPixels();
        let col = [0, 0, 0];

        let count = 0;

        for (let i = 0; i < 256; i++) {
            for (let x = 0; x < i; x++) {
                let index = (x + i * 256) * 4;

                let val = 0;

                for (let j = 0; j < 256; j++) {
                    val += this.record[i][x][j];
                }

                colFilter(val, col);

                this.image.pixels[index] = col[0];
                this.image.pixels[index + 1] = col[1];
                this.image.pixels[index + 2] = col[2];
                this.image.pixels[index + 3] = 255;

                // this.image.pixels[index] = val > this.threshold ? 256 : 0;
                // this.image.pixels[index + 1] = val > this.threshold ? 256 : 0;
                // this.image.pixels[index + 2] = val > this.threshold ? 256 : 0;
                // this.image.pixels[index + 3] = 255;

                // if(val > this.threshold){
                //     count++;
                //
                //     this.image.pixels[index] = col[0];
                //     this.image.pixels[index + 1] = col[1]
                //     this.image.pixels[index + 2] = col[2]
                //     this.image.pixels[index + 3] = 255;
                // } else {
                //     this.image.pixels[index] = 0;
                //     this.image.pixels[index + 1] = 0
                //     this.image.pixels[index + 2] = 0
                //     this.image.pixels[index + 3] = 255;
                // }
            }
        }

        if(count > 256*64) {
            this.threshold++;
        } else {
            this.threshold--;
        }

        console.log(this.threshold)

        this.image.updatePixels();
        image(this.image, 256, 256)
    }
}

module.exports = Recorder;
