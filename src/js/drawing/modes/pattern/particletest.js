class ParticleTest {
    constructor() {
        this.particles = [];
        this.amount = 1500;
        this.history = [];
        console.log(this);

        for (let i = 0; i < this.amount; i++) {
            let particle = [
                i,
                Math.random() * 1000,
                Math.random() * 1000,
                0,
                0,
            ];

            this.particles.push(particle);
        }
    }

    draw() {
        stroke(255);

        for (let i = 0; i < this.amount; i++) {
            let particle = this.particles[i];


            fill([Math.trunc(i / 16), Math.trunc(i / 16), Math.trunc(i / 4)]);

            square(particle[1] - 4, particle[2] - 4, 8, 8);
        }
    }

    move() {
        background(0);
        for (let i = 0; i < this.amount; i++) {
            let particle = this.particles[i];
            let total = Math.abs(particle[1] - 500) + Math.abs(particle[2] - 500);
            let ver = (particle[1] - 500) / total;
            let hor = (particle[2] - 500) / total;

            particle[3] -= ver / 500;
            particle[4] -= hor / 500;
        }

        let count = 0;

        for (let i = 0; i < this.amount; i++) {
            for (let j = 0; j < i; j++) {
                if (i === j) continue;

                let p1 = this.particles[i];
                let p2 = this.particles[j];

                let indexDis1 = Math.abs(i - j);
                let indexDis2 = Math.abs(j - i);
                let indexDisMin = indexDis1 < indexDis2 ? indexDis1 : indexDis2;

                let distance = Math.hypot(p1[1] - p2[1], p1[2] - p2[2]);
                let force = 0.25 / ((distance / 2) ** 2);
                if (force > 1) force = 1;

                let total = Math.abs(p1[1] - p2[1]) + Math.abs(p1[2] - p2[2]);
                let ver = (p1[1] - p2[1]) / total * force;
                let hor = (p1[2] - p2[2]) / total * force;

                if (indexDisMin < 16) {
                    ver -= ((p1[1] - p2[1]) / total) / 50000;
                    hor -= ((p1[2] - p2[2]) / total) / 50000;
                }

                p1[3] += ver;
                p1[4] += hor;
                p2[3] -= ver;
                p2[4] -= hor;

                if (indexDisMin < 16 && distance < 125) {
                    stroke([0, 255, 255])
                    line(p1[1], p1[2], p2[1], p2[2]);
                    count++;
                } else if (indexDisMin < 16 && distance < 150) {
                    stroke([0, (150-distance)*45, (150-distance)*45])
                    line(p1[1], p1[2], p2[1], p2[2]);
                    count++;
                }
            }
        }
        console.log(count);
        if (frameCount % 25) {
            this.history.push(count);
        }

        for (let i = 0; i < this.amount; i++) {
            let particle = this.particles[i];

            particle[1] += particle[3];
            particle[2] += particle[4];

            particle[3] *= 0.999;
            particle[4] *= 0.999;
        }
    }
}

module.exports = ParticleTest;
