class Cores {
    constructor(conf) {
        this._cores = [];
        this._amount = conf["cores_amount"] || 60;
        this._lowerLimit = conf["cores_lowerLimit"] || 80;
        this._upperLimit = conf["cores_upperLimit"] || 120;
        this._frameDuration = conf["frameDuration"] || 20; // in milliseconds
        this._windowMS = conf["windowMS"] || 4000; // in milliseconds


        this._getCores();
    }

    _getCores() {
        let timeStamps = this._windowMS / this._frameDuration;

        if (timeStamps - Math.ceil(timeStamps) !== 0) throw  new Error("this._windowMS / this._frameDuration must be a natural number");


        let step = (this._upperLimit - this._lowerLimit) / (this._amount - 1);

        for (let i = 0; i < this._amount; i++) {
            let bpm = this._lowerLimit + (step * i);
            let frequency = bpm / 60;
            let core = [];

            for (let j = 0; j < timeStamps; j++) {
                let seconds = this._windowMS / 1000;
                let stepsPerSecond = timeStamps / seconds;

                let val = Math.sin(j / stepsPerSecond * 2 * Math.PI * frequency);
                let scale = (0.8 + 0.2/ timeStamps * j);
                val *= scale;

                val = val < 0 ? 0 : val;

                core.push(val);
            }


            this._cores.push(core);
        }
    }

    get cores() {
        return this._cores;
    }
}

module.exports = Cores;
