class Cores {
    constructor(conf) {
        this._cores = [];
        this._amount = conf["cores_amount"] || 60;
        this._lowerLimit = conf["cores_lowerLimit"] || 80;
        this._upperLimit = conf["cores_upperLimit"] || 120;
        this._frameDuration = conf["frameDuration"] || 20; // in milliseconds
        this._windowMS = conf["windowMS"] || 4000; // in milliseconds


        this._getCores();
    }

    _getCores() {
        let currentBPM = this._lowerLimit;
        const increment = (this._upperLimit - this._lowerLimit) / (this._amount - 1);

        for (let i = 0; i < this._amount; i++) {

            let hz = currentBPM / 60;
            let core = [0];

            let framesPerTick = hz * 1000 / this._frameDuration;

            for (let j = 0; j < ; j++) {
                
            }

            this._cores.push({
                bpm: currentBPM,
                hz: hz,
                core:,
            })
        }
    }

    get cores() {
        return this._cores;
    }
}

module.exports = Cores;
