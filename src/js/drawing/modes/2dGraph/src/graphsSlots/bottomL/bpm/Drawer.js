class Drawer {
    constructor(conf, main) {
        this._main = main;
        this._modes = conf["drawMode"] || ["noveltyCurve"];
    }

    _clear() {
        fill(12);
        square(0, 772, 1024, 900);
        noFill()
    }

    draw() {
        this._clear();

        const modes = {
            noveltyCurve: this._noveltyCurve,
            cores: this._cores,
            result: this._result,
        }

        this._modes.forEach(mode => {

            modes[mode](this);
        })

    }

    _noveltyCurve(self) {
        let novelty = self._main._grabber.noveltyCurveMinusMean;

        stroke([255, 128, 128])

        beginShape()
        for (let i = 0; i < novelty.length; i++) {
            vertex(i * 2, 900 - (novelty[i] * 128))
        }
        endShape()
    }

    _cores(self) {
        let cores = self._main._cores.cores;

        stroke([128, 255, 128])

        let core_index = Math.ceil(frameCount / 20) % cores.length;

        beginShape()
        for (let i = 0; i < cores[core_index].length; i++) {
            vertex(i * 2, 900 - (cores[core_index][i] * 128))
        }
        endShape()
    }

    _result(self) {
        let history = self._main._detector.history;

        let sizeMultiply = Math.trunc(128 / history.length);



        for (let i = 0; i < history.length; i++) {
            let core_result = history[i];

            for (let j = 0; j < core_result.length; j++) {
                let val = core_result[j];
                stroke(val * 2550);

                line(j, 900 - (i * sizeMultiply), j, 900 - sizeMultiply - (i * sizeMultiply))
            }
        }
    }
}


module.exports = Drawer;
