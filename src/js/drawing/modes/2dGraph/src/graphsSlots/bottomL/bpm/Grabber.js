let history = require('../../../../../../../History');

class Grabber {
    constructor(conf) {
        this._frameDuration = conf["frameDuration"] || 20; // in milliseconds
        this._windowMS = conf["windowMS"] || 4000; // in milliseconds
        this._lastValues = [];
        this._noveltyCurve = [];
        this._frameValue = 0;
        this._lastTime = Date.now();
        this._lastFrameConsum = 0;
        this._recordFramesCount = this._windowMS / this._frameDuration;
    }

    get noveltyCurve() {
        return this._noveltyCurve;
    }

    get noveltyCurveMinusMean() {
        let sum = 0;

        this._noveltyCurve.forEach(val => {
            sum += val;
        });

        let mean = sum / this.noveltyCurve.length;

        let res = [];

        let highest = 0;

        for (let i = 0; i < this._noveltyCurve.length; i++) {
            let val = this._noveltyCurve[i] - mean;
            val = val < 0 ? 0 : val;

            if (val > highest) {
                highest = val;
            }

            res.push(val)
        }

        //normalize
        if (highest > 0.5) {
            for (let i = 0; i < res.length; i++) {
                res[i] /= (highest * 2);
            }
        }


        for (let i = 0; i < res.length; i++) {
            res[i] *= 4

            if (res[i] > highest) {
                highest = res[i];
            }
        }

        return res;
    }

    grab() {

        let slice = history.lastUnchanged;

        //calc mean
        let sum = 0;
        let lengthInSlice = 64;

        for (let i = 0; i < lengthInSlice; i++) {
            sum += Math.log(1 + slice[i] / 255);
        }

        let meanLog = sum / lengthInSlice;

        //get delta time
        let delta = Date.now() - this._lastTime;
        this._lastTime = Date.now();

        //add values to last value array
        while (delta > 0) {
            delta--;
            this._lastFrameConsum++;
            this._frameValue += meanLog / this._frameDuration;

            if (this._lastFrameConsum === this._frameDuration) {
                this._lastFrameConsum = 0;
                this._lastValues.push(this._frameValue);
                this._frameValue = 0;

                if (this._lastValues.length > 1) {
                    let length = this._lastValues.length
                    let novelty = this._lastValues[length - 1] - this._lastValues[length - 2];

                    novelty = novelty > 0 ? novelty : 0;


                    //this._noveltyCurve.push(novelty)
                    this._noveltyCurve.push(this._lastValues[length - 1])

                    this._cutDownNoveltyAndLast();
                }
            }
        }
    }

    _cutDownNoveltyAndLast() {
        while (this._noveltyCurve.length > this._recordFramesCount) {
            this._noveltyCurve.shift();
        }
        while (this._lastValues.length > this._recordFramesCount) {
            this._lastValues.shift();
        }
    }
}

module.exports = Grabber;
