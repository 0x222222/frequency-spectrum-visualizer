let Grabber = require("./Grabber")
let Drawer = require("./Drawer")
let Cores = require("./Cores")
let Detector = require("./Detector")

class Main {
    constructor(conf) {
        conf = conf || {};

        this._grabber = new Grabber(conf, this);
        this._drawer = new Drawer(conf, this);
        this._cores = new Cores(conf, this);
        this._detector = new Detector(conf, this);
    }

    calc() {
        this._grabber.grab();
        this._detector.detect();
    }

    draw() {
        this._drawer.draw();
    }
}


module.exports = new Main({
    drawMode: ["noveltyCurve", "cores", "result"],
    frameDuration: 20,
    cores_lowerLimit: 100,
    cores_upperLimit: 180,
    cores_amount: 32,
    detector_historyLength: 128,
    windowMS: 8000,
});

