let history = require('../../../../../../../History');

class Detector {
    constructor(conf, main) {
        this._main = main;

        this._history = [];
        this._detector_historyLength = conf["detector_historyLength"] || 64;
        this._history_withoutMean = [];
        this._history_copy = [];
        this._amount = conf["cores_amount"] || 60;

        for (let i = 0; i < this._amount; i++) {
            this._history.push([]);

            this._history_withoutMean.push(new Uint8Array(this._detector_historyLength));
            this._history_copy.push(new Uint8Array(this._detector_historyLength));
        }
    }


    get history() {
        return this._history;
    }

    get history_withoutMean() {

        for (let i = 0; i < this._amount; i++) {
            let sum = 0;

            for (let j = 0; j < this._history[i].length; j++) {
                sum += this._history[i][j];
            }

            let half_mean = sum / this._history[i].length / 2;


            for (let j = 0; j < this._history[i].length; j++) {
                let val = this._history[i][j] - half_mean;
                if (val < 0) val = 0;
                this._history_withoutMean[i][j] = val;
            }
        }

        return this._history_withoutMean;
    }

    get historyMiddle() {

    }

    _copy() {
        for (let i = 0; i < this._history.length; i++) {
            for (let j = 0; j < this._history[i].length; j++) {
                this._history_copy[i][j] = this.history[i][j];
            }
        }
    }

    detect() {
        let cores = this._main._cores.cores;
        let noveltyCurve = this._main._grabber.noveltyCurveMinusMean;


        for (let i = 0; i < cores.length; i++) {
            let core = cores[i];

            let sum = 0;

            for (let j = 0; j < core.length; j++) {
                sum += noveltyCurve[j] * core[j];
            }

            let mean = sum / core.length;

            this._history[i].push(mean);

            if (this._history[i].length > this._detector_historyLength) {
                this._history[i].shift();
            }
        }
    }
}

module.exports = Detector;
