let conf = require('../../../../../../Config').conf
let history = require('../../../../../../History');
let Filter = require("../../../../../../filter/index")


class BPM {
    constructor(conf) {
        conf = conf || {};
        this._windowMS = conf["windowMS"] || 4000;
        this._upperLimit = conf["upperLimit"] || 196;
        this._lowerLimit = conf["lowerLimit"] || 64;
        this._coreCount = conf["coreCount"] || 64;
        this._frameDuration = conf["frameDuration"] || 10; // in milliseconds
        this._recordFramesCount = this._windowMS / this._frameDuration;
        //this._tempogramFrames = conf["tempogramFrames"] || (this._recordFramesCount > 1024 ? 1024 : this._recordFramesCount); // in milliseconds

        //runtime
        this._noveltyCurve = [];
        this._noveltyCurveWithoutAverage = [];
        this._noveltyCurveAverage = [];
        this._lastValues = [];
        this._lastTime = Date.now();
        this._lastFrameConsum = 0; //how much of the last frame is already consumed by the previous frame
        this._frameValue = 0;

        this._cores = [];
        this._coresBPMList = [];
        this._getCores();
        this._tempogram = [];
    }

    _getCores() {
        //example
        //window size = 4000 ms => half = 2000 => 2000 / 10 = halfWindowFrameCount = 200 => ;
        let halfWindowSize = this._windowMS / 2;
        let halfWindowFrameCount = Math.trunc(halfWindowSize / this._frameDuration);

        //steps of bpm to test
        let step = (this._upperLimit - this._lowerLimit) / (this._coreCount - 1);

        for (let i = 0; i < this._coreCount; i++) {
            let core = [];

            for (let phaseShift = 0; phaseShift < 31; phaseShift++) {

                let phase = [];

                let currentBPM = Math.trunc(this._lowerLimit + step * i);
                this._coresBPMList.push(currentBPM);
                let frequency = currentBPM / 60;

                //create core
                for (let j = -halfWindowFrameCount; j < halfWindowFrameCount; j++) {


                    let val = Math.cos(Math.PI * j * 2 * frequency / 1000 * this._frameDuration - phaseShift / 5);
                    val = val < 0 ? 0 : val;
                    //reduce to the edge
                    if (j < 0) {
                        val = val / halfWindowFrameCount * (halfWindowFrameCount + j);
                    } else if (j > 0) {
                        val = val / halfWindowFrameCount * (halfWindowFrameCount - j);
                    }

                    //add on both sides of the array to get a symetic
                    phase.push(val);
                }

                core.push(phase);
            }


            this._cores.push(core);
        }
    }

    calcTempogram() {

        //remove first if limit is reached

        let window = this._cores[0].length * 2 - 1;

        if (this._noveltyCurve.length < window) {
            return;
        }
        this._tempogram = [];

        for (let i = 0; i < window; i++) {
            this._tempogram.push(this._calcTempogramSlice(this._cores[0][0].length / 2 - 1));
        }
    }

    calcTempogram123() {

        this._clearDrawArea();

        //remove first if limit is reached

        let window = this._cores[0].length * 2 - 1;
        let half_window = this._cores[0].length - 1;
        let curve = this._noveltyCurve;

        if (this._noveltyCurve.length < window) {
            return;
        }

        for (let coreIndex = 0; coreIndex < this._cores.length; coreIndex++) {
            let core = this._cores[coreIndex];
            let core_result = [];
            let core_result_sum = 0;

            for (let phaseShift = 0; phaseShift < core.length; phaseShift++) {
                let phase = core[phaseShift];

                let sum = 0;

                for (let i = 0; i < phase.length; i++) {
                    sum += curve[i] * phase[i];
                }

                let mean = sum / phase.length;


                core_result.push(mean);
                core_result_sum += mean;

            }


            let core_result_mean = core_result_sum / core.length;

            for (let phaseShift = 0; phaseShift < core.length; phaseShift++) {

                let val = core_result[phaseShift] - core_result_mean;

                if (val < 0) val = 0;

                stroke(val * 80000);
                line(512 + phaseShift, 900 - coreIndex * 4, 512 + phaseShift, 900 - coreIndex * 4 - 4);
            }
        }
    }

    removeAveragefromNoveltycurve() {

    }

    _calcTempogramSlice(index) {
        let slice = [];
        let curve = this._noveltyCurve;

        //iterate other all cores
        for (let coreIndex = 0; coreIndex < this._cores.length; coreIndex++) {
            let core = this._cores[coreIndex][0];


            //add mid value to sum
            let sum = 0;
            //todo faster when calc not count the amount
            let count = 0;

            for (let i = 1; i < core.length; i++) {

                count++;
                sum += curve[i] * core[i];


                if (isNaN(sum)) {
                    debugger;
                }
            }

            let mean = sum / (count);

            slice.push(mean);
        }

        //normalize
        let highest_v = slice[0];
        for (let i = 1; i < slice.length; i++) {
            if (highest_v < slice[i]) {
                highest_v = slice[i];
            }
        }

        if (highest_v !== 0) {
            for (let i = 0; i < slice.length; i++) {
                //slice[i] = slice[i] / highest_v;
                slice[i] = slice[i] * 255;
            }
        }

        return slice;
    }

    //takes an array and record and translate display frames into internal 10 ms frames
    record(slice) {
        slice = slice || history.lastUnchanged;

        //calc mean
        let sum = 0;

        for (let i = 0; i < slice.length; i++) {
            sum += Math.log(1 + slice[i] / 255);
        }

        let meanLog = sum / slice.length;

        //get delta time
        let delta = Date.now() - this._lastTime;
        this._lastTime = Date.now();

        //add values to last value array
        while (delta > 0) {
            delta--;
            this._lastFrameConsum++;
            this._frameValue += meanLog / this._frameDuration;

            if (this._lastFrameConsum === this._frameDuration) {
                this._lastFrameConsum = 0;
                this._lastValues.push(this._frameValue);
                this._frameValue = 0;

                if (this._lastValues.length > 1) {
                    let length = this._lastValues.length
                    let novelty = this._lastValues[length - 1] - this._lastValues[length - 2];

                    novelty = novelty > 0 ? novelty : 0;

                    this._noveltyCurve.push(novelty)

                    this._cutDownNoveltyAndLast();
                }
            }
        }
    }

    _cutDownNoveltyAndLast() {
        while (this._noveltyCurve.length > this._recordFramesCount) {
            this._noveltyCurve.shift();
        }
        while (this._lastValues.length > this._recordFramesCount) {
            this._lastValues.shift();
        }
    }

    //draws the novelty curve at the bottm
    drawNoveltyCurve() {
        let maxDraw = this._noveltyCurve.length > 1024 ? 1024 : this._noveltyCurve.length;
        let length = this._noveltyCurve.length;

        this._clearDrawArea();
        stroke([255, 0, 0])
        beginShape()
        for (let i = 0; i < this._noveltyCurve.length && i < 1024; i++) {
            let yPos = Math.trunc(900 - this._noveltyCurve[length - 1 - i] * 212);
            vertex(i, yPos);
        }
        endShape();
    }

    //draws the novelty curve at the bottm
    drawCores() {
        this._clearDrawArea();
        let core = this._cores[Math.trunc(frameCount / 31) % this._cores.length][frameCount % 31];

        if (frameCount % 31 === 0) {
            stroke([0, 0, 255])
        } else {
            stroke([0, 255, 0])
        }

        //beginShape()
        for (let i = 0; i < core.length && i < 512; i++) {
            let yPos = Math.trunc(900 - core[i] * 128);
            //vertex(i, yPos);
            line(512 + i, yPos, 512 + i, 900);
        }
        //endShape();
    }

    //clears the area there the function draws
    _clearDrawArea() {

        fill(12);
        square(0, 772, 1024, 900);
        noFill();
    }

    drawTempogram() {
        this._clearDrawArea();

        if (this._tempogram.length < 128) {
            return;
        }

        for (let i = 1; i <= 128; i++) {
            for (let j = 0; j < 16; j++) {
                stroke(this._tempogram[this._tempogram.length - i][j] * 64);
                line(i, 900 - (j * 8), i, 900 - (j * 8) - 8)
            }
        }
    }
}

module.exports = new BPM({
    frameDuration: 30,
    upperLimit: 200,
    lowerLimit: 100,
    coreCount: 64,
    windowMS: 4000,
});