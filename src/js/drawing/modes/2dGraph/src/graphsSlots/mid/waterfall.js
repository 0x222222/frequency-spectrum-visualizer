let conf = require('../../../../../../Config').conf
let history = require('../../../../../../History');

class waterfall {
    constructor() {
        this._panels = [];
        this._topPanel = undefined;
        this._offsetTop = 0;
        this.init();
    }

    init() {
        //create 4 start panel
        for (let x = 0; x < 6; x++) {
            //add panel
            this._panels.push({
                offset: 128 * x,
                image: new p5.Image(1024, 128),
            })


            //sad panel Black
            this._panels[x].image.loadPixels();

            for (let i = 0; i < 1024; i++) {
                for (let j = 0; j < 128; j++) {
                    this._panels[x].image.set(i, j, 0);
                }
            }


            this._panels[x].image.updatePixels();
        }

        //add Top Panel
        this._topPanel = new p5.Image(1024, 128);

        //Make Panel Black
        for (let i = 0; i < 1024; i++) {
            for (let j = 0; j < 128; j++) {
                this._topPanel.set(i, j, 0);
            }
        }
    }

    draw() {
        //increase offset
        for (let i = 0; i < 6; i++) {
            this._panels[i].offset++;
        }
        this._offsetTop++;


        //add panel if necessary
        if (this._offsetTop >= 128) {
            this._offsetTop = 0;
            this.addPanel();
        }


        const lastFilledColored = history.lastFilteredColored
        this._topPanel.loadPixels();


        //shift pixel
        for (let i = this._offsetTop; i > 0; i--) {
            for (let x = 0; x < 1024; x++) {
                let index = (x + i * this._topPanel.width) * 4;
                let prev = (x + (i - 1) * this._topPanel.width) * 4;

                this._topPanel.pixels[index] = this._topPanel.pixels[prev];
                this._topPanel.pixels[index + 1] = this._topPanel.pixels[prev + 1];
                this._topPanel.pixels[index + 2] = this._topPanel.pixels[prev + 2];
                this._topPanel.pixels[index + 3] = this._topPanel.pixels[prev + 3];
            }
        }


        //load Pixel;
        for (let x = 4; x < 1024; x++) {
            let index = x * 4;

            this._topPanel.pixels[index] = lastFilledColored[x][0];
            this._topPanel.pixels[index + 1] = lastFilledColored[x][1];
            this._topPanel.pixels[index + 2] = lastFilledColored[x][2];
            this._topPanel.pixels[index + 3] = 255;
        }

        //draw seconds
        let second_color = Math.round(Date.now() / 1000) % 2 === 0 ? 0 : 255;

        for (let x = 0; x < 4; x++) {
            let index = x * 4;
            this._topPanel.pixels[index] = second_color;
            this._topPanel.pixels[index + 1] = second_color;
            this._topPanel.pixels[index + 2] = second_color;
            this._topPanel.pixels[index + 3] = 255;
        }


        this._topPanel.updatePixels();

        //drawing

        for (let i = 0; i < 4; i++) {
            image(this._panels[i].image, 0, 256 + this._offsetTop + (128 * i));
        }

        image(this._topPanel, 0, 255);

        //draw background behind graph box above the waterfall
        this.drawTopGraphBoxBackground();

    }

    drawTopGraphBoxBackground() {
        fill(22, 22, 22);
        square(0, 0, 1024, 256);
        noFill();
        /*const lastFilledColored = history.lastFilteredColored

        /!*for (let x = 0; x < 1024; x++) {
            stroke(lastFilledColored[x])
            line(x,0,x,256)
        }*!/

        //

        const lineHeight = 1;
        let image_line = new p5.Image(1024, lineHeight);
        image_line.loadPixels();

        for (let x = 0; x < 1024; x++) {
            for (let i = 0; i < lineHeight; i++) {
                let index = (x + i * 1024) * 4;

                image_line.pixels[index] = lastFilledColored[x][0];
                image_line.pixels[index + 1] = lastFilledColored[x][1];
                image_line.pixels[index + 2] = lastFilledColored[x][2];
                image_line.pixels[index + 3] = 255;
            }
        }


        image_line.updatePixels();

        for (let y = 0; y < 256 / lineHeight; y++) {
            image(image_line, 0, y * lineHeight);
        }*/
    }

    addPanel() {
        this._panels.splice(5);

        let image = new p5.Image(1024, 128);
        this.cloneImage(image, this._topPanel);
        this._panels.unshift({
            image: image,
            offset: 0
        });

        for (let i = 0; i < this._panels.length; i++) {
            //this._panels[i].offset--
        }

        this._topPanel = new p5.Image(1024, 128);
    }

    cloneImage(target, source) {
        target.loadPixels();

        for (let x = 0; x < 1024; x++) {
            for (let i = 0; i < 128; i++) {
                let index = (x + i * target.width) * 4;

                target.pixels[index] = source.pixels[index];
                target.pixels[index + 1] = source.pixels[index + 1];
                target.pixels[index + 2] = source.pixels[index + 2];
                target.pixels[index + 3] = source.pixels[index + 3];
            }
        }

        target.updatePixels();
    }
}

module.exports = new waterfall();