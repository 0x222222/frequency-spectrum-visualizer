//require

//top
let frequencyGraph = require("./src/graphsSlots/top/frequencyGraph")


//mid
let waterfall = require("./src/graphsSlots/mid/waterfall")

//bottomL
let lowerGraph = require("./src/graphsSlots/bottomL/lowerGraph")
let bpm = require("./src/graphsSlots/bottomL/bpm")

//bottomS
let averageValues = require("./src/graphsSlots/bottomS/averageValues")


//side
let circleBox = require("./src/graphsSlots/side/circleBox")
let colorGradient = require("./src/graphsSlots/side/colorGradient")
let fadingGraph = require("./src/graphsSlots/side/fadingGraph")

//overlay
let overlay = require("./src/Overlay")


//Debugging
let DC = require("../../../helpingClasses/DebugClock")

//beat detection
//let bpm_v2  = require("./src/graphsSlots/bottomL/bpm/main")

//function
module.exports = function () {

    DC.start("WaterfallGraph");
    waterfall.draw();
    DC.stop("WaterfallGraph");

    DC.start("circleBox");
    circleBox.draw();
    DC.stop("circleBox");

    DC.start("colorGradient");
    colorGradient.draw();
    DC.stop("colorGradient");

    DC.start("fadingGraph");
    fadingGraph.draw();
    DC.stop("fadingGraph");

    DC.start("frequencyGraph");
    frequencyGraph.draw();
    DC.stop("frequencyGraph");

    DC.start("lowerGraph");
    lowerGraph.draw();
    //bpm_v2.calc();
    //bpm_v2.draw();
    // bpm.calcTempogram();
    // bpm.drawTempogram();
    // bpm.drawNoveltyCurve();
    DC.stop("lowerGraph");

    DC.start("averageValues");
    averageValues.draw();
    DC.stop("averageValues");

    overlay.draw();
}
