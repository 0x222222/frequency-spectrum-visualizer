let Record = require("./proceed/Record");
let Disturbance = require("./Disturbance");
let Pipe = require("./helpingClasses/Pipe");
let CyclicRecyclingArray = require("./helpingClasses/CyclicRecyclingArray");
let conf = require("./Config")
let Grabber = require("./proceed/Grabber");
let Piano = require("./proceed/Piano")
let Record8Line = require("./proceed/Record8Line");
let Filter = require("./filter/index");
let FilterManagment = require("./FilterManagment")


class History {
    constructor() {
        this.arr = [];
        this.last512 = new CyclicRecyclingArray(512, 1024, 0);
        this.current = -1;
        this._max = 512;
        this.image = undefined;
        this.record = new Record();

        this._lastUnchanged = new Int8Array(1024);


        this.peak = 0;
        this.highest = 0;

        for (let i = 0; i < this._max; i++) {
            let tmp = [];

            for (let j = 0; j < this._max; j++) {
                tmp.push(0)
            }

            this.arr.push(tmp);
        }
    }


    index(val) {
        return this.current + val > 0 ? this.current + val : this._max - 1;
    }

    valFilter(arr) {
        FilterManagment.applyFilters(arr);
        return arr;
    }

    disturbance() {
        //Disturbance.line(this.arr[this.current]);
    }

    get lastZoomed() {
        return this.arr[this.current];
    }

    get lastFiltered() {
        return this.arr[this.current];
    }

    get lastFilteredColored() {

        let arr = new Array(1024);
        let method = Filter.colors[conf.colorMode];
        for (let i = 0; i < arr.length; i++) {
            let rgb = [0, 0, 0];
            method(this.arr[this.current][i], rgb);
            arr[i] = rgb;
        }

        return arr;
    }

    get lastUnchanged() {
        return this._lastUnchanged;
    }

    add() {

        this.current++;

        //set _max
        if (this.current >= this._max) {
            this.current = 0;
        }


        this.disturbance();



        //standard filter
        //this.filter.zoomClear(this.arr[this.current], this.conf.zoom);
        Grabber.grab(this.arr[this.current]);

        //save unchanged arr
        for (let i = 0; i < 1024; i++) {
            this._lastUnchanged[i] = this.arr[this.current][i];
        }


        //Piano.detect(this.arr[this.current]);
        //Record8Line.addData(this.arr[this.current])
        //this.record.send(this.arr[this.current]);


        this.valFilter(this.arr[this.current]);

        this.last512.add(this.arr[this.current]);

        this.highest = 0;
        this.peak = 0;

        for (let i = 0; i < this.arr[this.current].length; i++) {
            if (this.arr[this.current][i] > this.highest) {
                this.peak = i;
                this.highest = this.arr[this.current][i];
            }
        }
    }



    get color() {
        return this.arr;
    }

    setNull() {
        console.log("Start set Null");
        let self = this;
        let i = 10;

        let interval = setInterval(() => {
            self.filter.setNull(fft.analyze());

            if (i-- <= 0) {
                console.log("Finished set Null");
                clearInterval(interval);
            }
        }, 100);
    }
}


module.exports = new History();