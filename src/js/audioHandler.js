const config = require('./Config')

class AudioHandler {

    /////////////////////constructor/////////////////////
    constructor() {
        this._currentDevice = undefined;
    }

    async setupAudio(ignoreDevice) {

        let deviceId = config.deviceId;
        let self = this;

        try {
            devices = await navigator.mediaDevices.enumerateDevices();
        } catch (e) {
            console.error("Could not enumerate devices")
        }

        // Second call to getUserMedia() with changed device may cause error, so we need to release stream before changing device
        if (window.stream) {
            stream.getAudioTracks()[0].stop();
        }


        FFTData = new Uint8Array(precision);
        //setup mic

        let constraints;

        let found = false;

        for (let i = 0; i < devices.length; i++) {
            if (deviceId === devices[i].deviceId) {
                found = true;
            }
        }

        if (found && !ignoreDevice) {
            constraints = {
                audio: {deviceId: {exact: deviceId}}
            }
        } else {
            constraints = {
                audio: true
            }
        }


        navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
            //navigator.mediaDevices.getUserMedia({audio: true}).then(function (stream) {

            aCtx = new (window.AudioContext || window.webkitAudioContext)();
            analyser = aCtx.createAnalyser();
            analyser.smoothingTimeConstant = 0;
            analyser.fftSize = precision * 2;
            microphone = aCtx.createMediaStreamSource(stream);
            microphone.connect(analyser);
        }, error => {
            if (!ignoreDevice) {
                self.setupAudio(true);
            } else {
                throw error;
            }
        });
    }

    async getDevices() {
        devices = await navigator.mediaDevices.enumerateDevices();
        devices.forEach((entry, index) => {
            entry["index"] = index;
            //entry.label += "#" + entry.deviceId.slice(0,8);
        })

        return devices;
    }

    async getDevicesReadable() {
        let res = [];
        devices = await navigator.mediaDevices.enumerateDevices();
        devices.forEach(entry => {
            res.push(entry.label);
        })

        return res;
    }

    changeDevice(deviceId) {
        config.deviceId = deviceId;
        config.reload();
    }

    getCurrentDeviceID() {

    }

    /////////////////////Methods/////////////////////
    /////////////////////GETTER/////////////////////
    /////////////////////SETTER/////////////////////
}

module.exports = new AudioHandler();