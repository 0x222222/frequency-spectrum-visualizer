// p5 = require('./lib/p5');
let history = require("./History");
let conf = require("./Config");
let DebugInfo = require("./DebugInfo");

let drawParts = require("./drawing/drawParts");

draw = () => {
    if (analyser && !conf["stop"]) {


        history.add();
        conf.refreshRuntimeVariables();


        //lowerBox
        drawParts();
        DebugInfo.measure();

        //print debug clock
        /*if (frameCount % 100 === 0) {
            console.clear()
            require("./helpingClasses/DebugClock").print();
        }*/
    }
}

let iteration = 0;
let waitForCanvas = setInterval(()=>{
    if(document.getElementById('defaultCanvas0')){


        setTimeout(() => {

            if (iteration === 0) {
                //init
                require('./Init')();
            }

            try {
                let a = document.getElementById('box');

                document.getElementById('box').appendChild(document.getElementById('defaultCanvas0'));
                document.body.style.visibility = "visible";
                clearTimeout(waitForCanvas);
            } catch (e) {
                console.error(e);
                window.location.reload()
            }
        }, 500);

    }
},100)

setTimeout(() => {

    if(document.getElementById('defaultCanvas0')==null){
        window.location.reload()
    }

}, 3500);




