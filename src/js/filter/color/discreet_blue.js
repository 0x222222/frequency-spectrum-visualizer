const colors = [
    "004c6d",
    "255e7e",
    "3d708f",
    "5383a1",
    "6996b3",
    "7faac6",
    "94bed9",
    "abd2ec",
    "c1e7ff"
]

function hexToRgb(hex) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? [
        parseInt(result[1], 16),
        parseInt(result[2], 16),
        parseInt(result[3], 16)
    ] : null;
}

for (let i = 0; i < colors.length; i++) {
    colors[i] = hexToRgb(colors[i]);
}


module.exports = (val, rgb) => {

    let index = Math.trunc(val / Math.ceil(256 / colors.length));

    rgb[0] = colors[index][0];
    rgb[1] = colors[index][1];
    rgb[2] = colors[index][2];
}