const colors = [
    "020003",
    "040005",
    "060007",
    "080009",
    "09000A",
    "0B000C",
    "0D000E",
    "0F0010",
    "110012",
    "130014",
    "150016",
    "170018",
    "19001A",
    "1B001C",
    "1D001E",
    "200020",
    "220022",
    "240024",
    "260026",
    "280028",
    "2A002A",
    "2C002C",
    "2E002E",
    "300030",
    "320032",
    "340034",
    "360036",
    "380038",
    "3A003A",
    "3C003C",
    "3E003E",
    "400040",
    "420042",
    "440044",
    "460046",
    "480048",
    "4A004A",
    "4C004C",
    "4E004E",
    "500050",
    "510051",
    "530053",
    "550055",
    "570057",
    "590059",
    "5B005B",
    "5D005D",
    "5F005F",
    "610061",
    "630063",
    "650065",
    "670067",
    "690069",
    "6B006B",
    "6D006D",
    "6F006F",
    "710171",
    "730173",
    "750175",
    "770277",
    "790279",
    "7B037B",
    "7D037D",
    "7F037F",
    "810481",
    "830483",
    "850585",
    "870587",
    "890689",
    "8B068B",
    "8D078D",
    "8F078F",
    "910891",
    "930893",
    "950995",
    "970997",
    "980A98",
    "9A0B9A",
    "9C0B9C",
    "9E0C9E",
    "A00DA0",
    "A20DA2",
    "A40EA4",
    "A60FA6",
    "A80FA7",
    "AA10A7",
    "AC11A8",
    "AE11A8",
    "B012A9",
    "B213A9",
    "B414AA",
    "B614AA",
    "B815AB",
    "BA16AB",
    "BC17AC",
    "BE17AC",
    "C018AC",
    "C219AD",
    "C41AAD",
    "C61BAE",
    "C81CAE",
    "CA1DAE",
    "CC1EAE",
    "CE1EAF",
    "D01FAF",
    "D220AF",
    "D421B0",
    "D622B0",
    "D823B0",
    "DA24B0",
    "DC25B1",
    "DE26B1",
    "DF27B1",
    "E128B1",
    "E329B1",
    "E52AB1",
    "E72BB2",
    "E92CB2",
    "EB2EB2",
    "ED2FB2",
    "EF30B2",
    "F131B2",
    "F332B2",
    "F533B3",
    "F734B3",
    "F935B3",
    "FB37B3",
    "FD38B3",
    "FF39B3",
    "FF3AB6",
    "FF3BB8",
    "FF3BBB",
    "FF3CBD",
    "FF3DC0",
    "FF3EC2",
    "FF3FC5",
    "FF3FC7",
    "FF40CA",
    "FF41CC",
    "FF42CF",
    "FF43D1",
    "FF44D4",
    "FF45D6",
    "FF46D9",
    "FF47DB",
    "FF48DD",
    "FF49E0",
    "FF4AE2",
    "FF4BE5",
    "FF4CE7",
    "FF4DE9",
    "FF4EEC",
    "FF4FEE",
    "FF50F0",
    "FF51F3",
    "FF52F5",
    "FF53F7",
    "FF54F9",
    "FF55FB",
    "FF56FE",
    "FF57FF",
    "FF59FF",
    "FF5AFF",
    "FF5BFF",
    "FF5CFF",
    "FF5DFF",
    "FF5EFF",
    "FF60FF",
    "FF61FF",
    "FF62FF",
    "FF63FF",
    "FF65FF",
    "FF66FF",
    "FF67FF",
    "FF69FF",
    "FF6AFF",
    "FF6BFF",
    "FF6DFF",
    "FF6EFF",
    "FF6FFF",
    "FF71FF",
    "FF72FF",
    "FF73FF",
    "FF75FF",
    "FF76FF",
    "FF78FF",
    "FF79FF",
    "FF7BFF",
    "FF7CFF",
    "FF7EFF",
    "FE7FFF",
    "FC81FF",
    "FB82FF",
    "FA84FF",
    "F985FF",
    "F887FF",
    "F788FF",
    "F68AFF",
    "F58BFF",
    "F58DFF",
    "F48FFF",
    "F390FF",
    "F292FF",
    "F194FF",
    "F096FF",
    "F098FF",
    "EF9AFF",
    "EE9CFF",
    "EE9EFF",
    "EDA0FF",
    "ECA2FF",
    "ECA4FF",
    "EBA6FF",
    "EBA8FF",
    "EAAAFF",
    "EAACFF",
    "E9AEFF",
    "E9B0FF",
    "E9B2FF",
    "E8B4FF",
    "E8B5FF",
    "E8B7FF",
    "E8B9FF",
    "E8BBFF",
    "E7BDFF",
    "E7BFFF",
    "E7C1FF",
    "E7C3FF",
    "E7C5FF",
    "E8C7FF",
    "E8C9FF",
    "E8CBFF",
    "E8CDFF",
    "E8CFFF",
    "E9D1FF",
    "E9D3FF",
    "EAD5FF",
    "EAD7FF",
    "EBD9FF",
    "EBDBFF",
    "ECDDFF",
    "ECDFFF",
    "EDE1FF",
    "EEE3FF",
    "EFE5FF",
    "F0E7FF",
    "F1E9FF",
    "F2EBFF",
    "F3EDFF",
    "F4EFFF",
    "F5F1FF",
    "F6F3FF",
    "F7F5FF",
    "F9F7FF",
    "FAF9FF",
    "FBFBFF",
    "FDFCFF"
]

function hexToRgb(hex) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? [
        parseInt(result[1], 16),
        parseInt(result[2], 16),
        parseInt(result[3], 16)
    ] : null;
}

for (let i = 0; i < colors.length; i++) {
    colors[i] = hexToRgb(colors[i]);
}


module.exports = (val, rgb) => {
    val = Math.trunc(val);

    rgb[0] = colors[val][0];
    rgb[1] = colors[val][1];
    rgb[2] = colors[val][2];
}