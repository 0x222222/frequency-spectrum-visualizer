const colors = [
    "00876c",
    "439981",
    "6aaa96",
    "8cbcac",
    "aecdc2",
    "cfdfd9",
    "f1f1f1",
    "f1d4d4",
    "f0b8b8",
    "ec9c9d",
    "e67f83",
    "de6069",
    "d43d51",
]

function hexToRgb(hex) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? [
        parseInt(result[1], 16),
        parseInt(result[2], 16),
        parseInt(result[3], 16)
    ] : null;
}

for (let i = 0; i < colors.length; i++) {
    colors[i] = hexToRgb(colors[i]);
}


module.exports = (val, rgb) => {

    let index = Math.trunc(val / Math.ceil(256 / colors.length));

    rgb[0] = colors[index][0];
    rgb[1] = colors[index][1];
    rgb[2] = colors[index][2];
}