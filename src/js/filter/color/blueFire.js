module.exports = (val, rgb) => {
    if (val < 160) {
        rgb[0] = rgb[1] = rgb[2] = val / 10;
    } else if (val < 240) {
        rgb[1] = rgb[2] = 16;
        rgb[2] = (val - 160) * 2 + 16
    } else {
        rgb[2] = 160 + (val - 240) * 5 + 16
        rgb[1] = rgb[0] = 16 + (val - 240) * 11;
    }
}
