const colors = [
    "000000",
    "003f5c",
    "374c80",
    "7a5195",
    "bc5090",
    "ef5675",
    "ff764a",
    "ffa600",
]

function hexToRgb(hex) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? [
        parseInt(result[1], 16),
        parseInt(result[2], 16),
        parseInt(result[3], 16)
    ] : null;
}

for (let i = 0; i < colors.length; i++) {
    colors[i] = hexToRgb(colors[i]);
}


module.exports = (val, rgb) => {

    let index = Math.trunc(val / Math.ceil(256 / colors.length));

    rgb[0] = colors[index][0];
    rgb[1] = colors[index][1];
    rgb[2] = colors[index][2];
}