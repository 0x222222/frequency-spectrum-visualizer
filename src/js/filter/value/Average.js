class Average extends require("./Root") {
    constructor() {
        super();

        this.properties.dpName = "Average";
        this.properties.name = "Average";
        this.properties.inputs = {}
    }

    calc(arr) {
        let sum = 0;

        for (let i = 0; i < arr.length; i++) {
            sum += arr[i];
        }

        let mean = sum / arr.length;

        for (let i = 0; i < arr.length; i++) {
            arr[i] = mean;
        }
    }
}

module.exports = Average