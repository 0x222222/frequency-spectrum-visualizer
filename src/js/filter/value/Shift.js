class Shift extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = true;
        this.properties.dpName = "Shift";
        this.properties.name = "Shift";
        this.properties.inputs = {
            distance: {
                dpName: "Amount",
                type: "number",
                min: -1024,
                max: 1024,
                value: 0,
            }
        }
    }

    calc(arr) {
        let distance = parseFloat(this.properties.inputs.distance.value);

        if(distance===0){
            return;
        }


        if(distance>0){

            for (let i = arr.length-1; i >=distance; i--) {
                arr[i] = arr[i-distance];
            }
            for (let i = 0; i < distance; i++) {
                arr[i] = 0;
            }
        }else {
            for (let i = 0; i < arr.length+distance; i++) {
                arr[i] = arr[i-distance];
            }
            for (let i = arr.length+distance; i < arr.length; i++) {
                arr[i] = 0;
            }
        }


    }
}

module.exports = Shift