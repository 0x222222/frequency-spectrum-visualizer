class MakeBin extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = true;
        this.properties.dpName = "Make binary";
        this.properties.name = "MakeBin";
        this.properties.inputs = {
            threshold: {
                dpName: "Threshold ",
                type: "number",
                min: 0,
                max: 255,
                value: 128,
            }
        }
    }

    calc(arr) {
        let threshold = parseFloat(this.properties.inputs.threshold.value);

        for (let i = 0; i < arr.length; i++) {
            if (arr[i] < threshold) {
                arr[i] = 0;
            } else {
                arr[i] = 255;
            }
        }
    }
}

module.exports = MakeBin