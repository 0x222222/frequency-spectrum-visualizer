class CutFollowingValues extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = false;
        this.properties.dpName = "Cut following n values";
        this.properties.name = "CutFollowingValues";
        this.properties.inputs = {
            returnSpeed: {
                dpName: "n",
                type: "number",
                min: 1,
                max: 64,
                value: 2,
            }
        }

        this._cooldown = new Int32Array(1024);

        this._cooldown.forEach((v, i, a) => {
            a[i] = 0;
        })
    }

    calc(arr) {
        let cooldown = parseFloat(this.properties.inputs.returnSpeed.value);

        for (let i = 0; i < arr.length; i++) {
            if (this._cooldown[i]<=0) {
                this._cooldown[i] = cooldown;
            } else {
                arr[i] = 0;
            }
        }

        this._cooldown.forEach((v, i, a) => {
            if (a[i] > 0) {
                a[i]--;
            } else {
                arr[i] = 0;
            }
        })
    }
}

module.exports = CutFollowingValues