class Multiply extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = true;
        this.properties.dpName = "Multiply";
        this.properties.name = "Multiply";
        this.properties.inputs = {
            factor: {
                dpName: "Factor",
                type: "number",
                min: 0,
                max: 16,
                value: 2,
            }
        }
    }

    calc(arr) {
        let factor = parseFloat(this.properties.inputs.factor.value);

        for (let i = 0; i < arr.length; i++) {
            arr[i] *= factor;
            if (arr[i] > 255) {
                arr[i] = 255;
            }
        }
    }
}

module.exports = Multiply