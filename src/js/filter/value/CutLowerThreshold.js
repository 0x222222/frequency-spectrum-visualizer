class CutLowerThreshold extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = true;
        this.properties.dpName = "Cut values below threshold";
        this.properties.name = "CutLowerThreshold";
        this.properties.inputs = {
            threshold: {
                dpName: "Threshold (<)",
                type: "number",
                min: 0,
                max: 255,
                value: 128,
            }
        }
    }

    calc(arr) {
        let threshold = parseFloat(this.properties.inputs.threshold.value);

        for (let i = 0; i < arr.length; i++) {
            if (arr[i] < threshold) {
                arr[i] = 0;
            }
        }
    }
}

module.exports = CutLowerThreshold