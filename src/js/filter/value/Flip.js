class Flip extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = false;
        this.properties.dpName = "Flip";
        this.properties.name = "Flip";
    }

    calc(arr) {
        for (let i = 0; i < 512; i++) {
            let temp = arr[i];
            arr[i] = arr[1023-i];
            arr[1023-i] = temp;
        }
    }
}

module.exports = Flip