class Contours extends require("./Root") {
    constructor() {
        super();

        this.properties.dpName = "Contours";
        this.properties.name = "Contours";
        this.properties.inputs = {}
        this._last = new Uint8Array(1024);
        this._current = new Uint8Array(1024);
    }

    calc(arr) {


        for (let i = 1; i < arr.length - 1; i++) {
            if (
                Math.trunc(arr[i - 1] / 128) !== Math.trunc(arr[i + 1] / 128) ||
                Math.trunc(arr[i + 1] / 128) !== Math.trunc(this._last[i] / 128) ||
                Math.trunc(arr[i] / 128) !== Math.trunc(this._last[i - 1] / 128) ||
                Math.trunc(arr[i] / 128) !== Math.trunc(this._last[i + 1] / 128)
            ) {
                this._current[i] = 0;
            } else {
                this._current[i] = arr[i];
            }

            this._last[i] = arr[i];
        }

        for (let i = 0; i < 1024; i++) {
            arr[i] = this._current[i];
        }
    }
}

module.exports = Contours