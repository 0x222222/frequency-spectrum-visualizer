class ConvolutionSharp extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = true;
        this.properties.dpName = "Convolution Sharpen";
        this.properties.name = "ConvolutionSharp";
        this.properties.inputs = {
            strength: {
                dpName: "Strength",
                type: "number",
                min: 0,
                max: 5,
                value: 1,
            }
        }

        this._tempArray = new Int32Array(1024);

        this._cores = [
            [1],
            [-1, 3, -1],
            [-1, -2, 7, -2, -1],
            [-1, -2, -3, 13, -3, -2, -1],
            [-0.5, -1, -1.5, -2, -2.5, 15, -2.5, -2, -1.5, -1, -0.5],
            [-1, -2, -3, -4, 20, -4, -3, -2, -1],
        ]
    }

    calc(arr) {

        let core = this._cores[parseInt(this.properties.inputs.strength.value)];
        let coreSizeHalf = core.length;
        let coreSize = Math.floor(core / 2);


        //copy array
        let tmpArr = this._tempArray;
        for (let i = 0; i < arr.length; i++) {
            tmpArr[i] = arr[i];
        }

        let prev = arr[0]

        for (let i = 0; i < arr.length; i++) {

            let sum = 0;

            for (let j = 0; j < core.length; j++) {
                let index = i - coreSizeHalf + j;
                if (index < 0 || index > 1024) {
                    continue;
                }

                sum += tmpArr[index] * core[j];
            }

            if (sum < 0) sum = 0;
            if (sum > 255) sum = 255;

            arr[i] = sum;
        }
    }
}

module.exports = ConvolutionSharp;