let conf = require('./Config');
let audioHandler = require('./audioHandler')

p5.disableFriendlyErrors = true;

console.log("Init")

function setup() {



    if (conf.threeD) {
        createCanvas(1300, 900, WEBGL);
        setAttributes('antialias', true);

        noFill();
        box(50);
    } else {
        createCanvas(1300, 900);
        setAttributes('antialias', true);
    }

    noFill();


    audioHandler.setupAudio();
}

module.exports = setup;