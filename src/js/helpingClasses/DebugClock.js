class DebugClock {

    /////////////////////constructor/////////////////////
    constructor() {
        this._start = Date.now();
        console.log({DebugClock: this})

        this._entriesTotalTime = {};
        this._entriesLastTime = {};
        this._entriesCalled = {}; //check if entries is started but not stopped
    }


    /////////////////////Methods/////////////////////
    start(name) {
        if (!name) return;
        if (!this._entriesLastTime[name]) {
            this._entriesLastTime[name] = Date.now();
            this._entriesTotalTime[name] = 0;
        }

        this._entriesLastTime[name] = Date.now();
        this._entriesCalled[name] += 1;
    }

    stop(name) {
        if (!name) return;
        if (!this._entriesLastTime[name]) {
            this._entriesLastTime[name] = Date.now();
            this._entriesTotalTime[name] = 0;
        }

        this._entriesTotalTime[name] += Date.now() - this._entriesLastTime[name];
        this._entriesCalled[name] -= 1;
    }

    print() {
        let missingStop =false;

        console.log("Total time: " + Math.round((Date.now() - this._start) / 1000))

        for (let name in this._entriesLastTime) {
            console.log("Name: " + name + " | Total Time: " + this.consumedTimeTotal(name) + " | Relative Time: " + this.consumedTimeRelative(name));

            if(this._entriesCalled[name] > 1){
                console.warn("Entry ist stared but not stopped");
                missingStop = true;
            }
        }

        if(!missingStop){
            console.log("No missing stop")
        }

    }

    consumedTimeTotal(name) {
        return Math.round(this._entriesTotalTime[name] / 1000);
    }

    consumedTimeRelative(name) {
        let totalTime = Date.now() - this._start;
        return Math.round(100 / totalTime * this._entriesTotalTime[name])
    }

    /////////////////////GETTER/////////////////////
    /////////////////////SETTER/////////////////////
}

module.exports = new DebugClock();