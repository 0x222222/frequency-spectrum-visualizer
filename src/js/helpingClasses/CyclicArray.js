let _ = require('lodash')

class CyclicArray {

    /////////////////////constructor/////////////////////
    constructor(size, defaultValue) {

        this._size = size || 100;
        this._arr = new Array(this._size);

        if (defaultValue) {
            for (let i = 0; i < this._arr.length; i++) {
                this._arr[i] = _.cloneDeep(defaultValue);
            }
        }

        //runtime
        this._current = 0;
    }

    /////////////////////Methods/////////////////////
    add(data) {
        this._current++;

        if (this._current >= this._size) {
            this._current = 0;
        }

        this._arr[this._current] = data;
    }

    forEach(method) {
        this._arr.forEach((value, index, array) => {
            method(value, index, array);
        });
    }

    getByOffset(offset) {
        if (offset > this._arr.length) {
            offset = this._arr.length - 1;
        }

        let index = this._current - offset;

        if (index < 0) {
            index = this._arr.length + index;
        }

        return this._arr[index];
    }

    /////////////////////GETTER/////////////////////
    get last() {
        return this._arr[this._current];
    }

    get length() {
        return this._arr.length;
    }

    /////////////////////SETTER/////////////////////
}

module.exports = CyclicArray;