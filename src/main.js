

import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueSweetalert2 from 'vue-sweetalert2';

//drag and drop
import VueDragDrop from 'vue-drag-drop';
Vue.use(VueDragDrop);

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2, {
    background: "#151515"
});

Vue.config.productionTip = true;

// register the plugin on vue
import Toasted from 'vue-toasted';

Vue.use(Toasted, {
    duration: 4000,
    iconPack: "mdi"
});

new Vue({
    vuetify,
    render: function (h) {
        return h(App)
    }
}).$mount('#app');
